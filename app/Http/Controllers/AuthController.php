<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use SendGrid\Mail\Mail;
use DB;

class AuthController extends Controller
{
    //index login
    public function index_login(){
        return response ([
                'message' => 'Un autorized'
        ], 401);
    }
    //
    public function login(Request $request) {
//        $fields = $request->validate([
//    		'post' => 'required|string'
//    	]);
        $first = substr($request->post, 0, 1);
        if ($first == '0') {
            $phone = $request->code_phone.substr($request->post,1);
        } else {
            $phone = $request->code_phone.$request->post;
        }

        //masak gk berubah
    	$user = User::where('user_phone',$phone)->first();

    	if (!$user) {

            $message = "Nomor Anda tidak terdaftar";

    		return response ([
	    		'message' => $message
	    	], 500);
    	}
        $code = $this->generateRandomString(5,1);
        $updsate_token = User::where('user_phone',$phone)->update(['otp'=>$code,'otp_request_time'=> date('Y-m-d H:i:s')]);
        if ($user->register_method == 0) {
            $message = "Masuka Kode Verifikasi: ".$code.". untuk melanjutkan proses registrasi Anda";
            $send_wa = parent::ZenzifaWa($user->user_phone,$message);
        } else {
            $message = "Silahkan Masukkan : ".$code.". untuk melanjutkan proses registrasi Anda";
            $send_wa = parent::ZenzifaSms($user->user_phone,$code);
        }

    	$response = [
            'id'  => $user->id,
    		'otp' => $code
    	];
    	parent::LogSystem('Berhasil Login Ke System',$user->id,'Login',1,null);

    	return response($response,200);
    }

    //
    public function login_sosmed(Request $request) {
        try {
            $fields = $request->validate([
                'id' => 'required|string',
                'type' => 'required|integer'
            ]);
            $user = User::where('social_account_id',$request->id)->where('register_method',$request->type)->first();
            if ($user != null) {
                $update =  User::where('social_account_id',$request->id)->update(['user_name' => $request->name,'user_email' => $request->email]);
                parent::LogSystem('Berhasil Login Ke System BY Sosmed',$user->id,'Login',1,null);
                $token = $user->createToken('myapptoken')->plainTextToken;
                $response = [
                    'token' => $token,
                    'name'  => $user->user_name
                ];
                return response($response,200);
            } else {
                $insert_user = User::insertGetId([
                    'social_account_id' => $request->id,
                    'register_method' => $request->type,
                    'user_name' => $request->name,
                    'user_email' => $request->email,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                $user = User::where('social_account_id',$request->id)->where('register_method',$request->type)->first();
                parent::LogSystem('Berhasil Register BY Sosmed',$user->id,'Login',1,null);

                $token = $user->createToken('myapptoken')->plainTextToken;
                $response = [
                    'token' => $token,
                    'name'  => $user->user_name
                ];
                return response($response,200);
            }
        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }

    }

    public function register(Request $request) {
        try {

//        	$fields = $request->validate([
//                'type' => 'required|integer',
//                'post' => 'required|string'
//            ]);
            $first = substr($request->post, 0, 1);
            if ($first == '0') {
                $phone = $request->code_phone.substr($request->post, 1);
            } else {
                $phone = $request->code_phone.$request->post;
            }

            if ($request->type == 1) {

                //Creste User
                $user = $this->CreateUser($request);

                //Send Kode OTP Via WA
                if ($user['code'] == 200) {

                    $message = "Silahkan Masukkan : ".$user['otp'].". untuk melanjutkan proses registrasi Anda";

                    $send_wa = parent::ZenzifaSms($phone,$user['otp']);
                    $response = [
                        'id' => $user['id'],
                        'otp' => $user['otp']
                    ];
                    parent::LogSystem($phone.' Berhasil Melakukan Registrasi',$user['id'],'Registrasi',1,null);

                    return response($response,200);
                } else {
                    return response(['message' =>$user['message']],$user['code']);
                }

            } else {
                //Create User
                $user = $this->CreateUser($request);
                //dd($user['code']);

                //Send Kode OTP Via WA
                if ($user['code'] == 200) {
                    //Send Kode OTP Via WA
                    $message = "Masuka Kode Verifikasi: ".$user['otp'].". untuk melanjutkan proses registrasi Anda";

                    $send_wa = parent::ZenzifaWa($phone,$message);
                    // CREATE USER

                    $response = [
                        'id' => $user['id'],
                        'otp' => $user['otp']
                    ];
                    parent::LogSystem($phone.' Berhasil Melakukan Registrasi',$user['id'],'Registrasi',1,null);

                    return response($response,200);
                } else {
                    return response(['message' =>$user['message']],$user['code']);
                }
            }
        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }

    }

    public function CreateUser($request){
        $return['code'] = 500;
        $first = substr($request->post, 0, 1);
        if ($first == '0') {
            $phone = $request->code_phone.substr($request->post, 1);
        } else {
            $phone = $request->code_phone.$request->post;
        }
        $cek = User::where('user_phone',$phone)->get();
        if ($cek->isNotEmpty()) {

            if ($cek[0]->is_valid_register == 1) {
                $return['message'] = 'Maaf Nomor HP tersebut sudah digunakan';
            } else {
                $code = $this->generateRandomString(5,1);
                $user = User::where('id',$cek[0]->id)->update([
                    'user_phone' => $phone,
                    'user_name' => $request->user_name,
                    'farmer_name' => $request->farmer_name,
                    'register_method' => $request->type,
                    'otp'  => $code,
                    'otp_request_time'=> date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                $return['id']   = $cek[0]->id;
                $return['otp']  = $code;
                $return['code'] = 200;

            }
        } else {
            $code = $this->generateRandomString(5,1);
            $user = User::insertGetId([
                'user_phone' => $phone,
                'user_name' => $request->user_name,
                'farmer_name' => $request->farmer_name,
                'register_method' => $request->type,
                'otp'  => $code,
                'otp_request_time'=> date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            $return['id']   = $user;
            $return['otp']  = $code;
            $return['code'] = 200;
        }
        return $return;
    }

    public function otp_validation(Request $request) {
        try {
            $user = User::where('id',$request->id)->where('otp',$request->otp)->first();
            $cek_data = User::where('id',$request->id)->first();

            if ($cek_data->send_otp_number >= 100) {
                return response([
                    'message' => 'Request OTP Maksimal 100 kali dalam 1 menit',
                ], 500);
            }
            $up_number = $cek_data->send_otp_number + 1;
            $update_otp = DB::table('t_users')->where('id',$request->id)->update(['send_otp_number'=>$up_number]);
            if ($user != null) {

                date_default_timezone_set("Asia/Jakarta");
                $time = new \DateTime($user->otp_request_time);
                $diff = $time->diff(new \DateTime());
                $minutes = ($diff->days * 24 * 60) +
                           ($diff->h * 60) + $diff->i;
                //dd($minutes);
                if ($minutes > 2){

                    return response(['message' => 'Maaf OTP Anda Sudah Kadaluarsa'],500);
                } else {
                    $update_otp = DB::table('t_users')->where('id',$request->id)->update(['send_otp_number'=>0,'request_otp_number'=>0]);
                    $valid = User::where('id',$request->id)->update(['is_valid_register'=>1]);
                    $token = $user->createToken('myapptoken')->plainTextToken;

                    $response = [
                        'token' => $token
                    ];
                    return response($response,200);
                }

            } else {
                return response(['message' => 'Maaf OTP Anda Salah'],500);
            }

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }
    }

    public function send_otp(Request $request) {
        try {
            $code = $this->generateRandomString(5,1);
            //
            $cek = DB::table('t_users')->where('id',$request->id)->select('id','request_otp_number')->get();
            if ($cek[0]->request_otp_number >= 100) {
                return response([
                    'message' => 'Request OTP Maksimal 100 kali dalam sehari',
                ], 500);
            }

            $number = $cek[0]->request_otp_number + 1;
            $update_number = DB::table('t_users')->where('id',$request->id)->update(['request_otp_number'=>$number,'send_otp_number'=>0,'otp'=>$code,'otp_request_time'=> date('Y-m-d H:i:s')]);
            //
            $user = User::where('id',$request->id)->first();
            $response = [
                'id' => $request->id,
                'message' => 'Silahkan Masukkan OTP Anda'
            ];

            if ($user->register_method == 1) {
                $message = "Silahkan Masukkan : ".$code.". untuk melanjutkan proses registrasi Anda";
                $send_sms = parent::ZenzifaSms($user->user_phone,$code);
            } else {
                $message = "Masukan Kode Verifikasi: ".$code.". untuk melanjutkan proses registrasi Anda";
                $send_wa = parent::ZenzifaWa($user->user_phone,$message);
            }


            return response($response,200);

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }
    }

    public function update_name(Request $request) {
        try {
            $update = User::where('id',auth()->user()->id)->update(['user_name'=>$request->name]);
            $response = [
                'message' => "Berhasil Update nama",
            ];
            return response($response,200);

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }

    }

    public function update_pin(Request $request) {
        try {
            if ($request->pin != $request->pin_confirmation) {
                return response([
                    'message' => 'PIN tidak sama '
                ],500);
            }
            $update = User::where('id',auth()->user()->id)->update(['pin_code'=>Hash::make($request->name)]);
            $response = [
                'message' => "Berhasil Update PIN"
            ];
            return response($response,200);

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }

    }

    public function generateRandomString($length,$type) {
    	if ($type == 1) {
    		$characters = '0123456789';
    	} else {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	}
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function logout(Request $request){
        parent::LogSystem('Berhasil Log out dari System',auth()->user()->id,'Log out',1,null);
		auth()->user()->tokens()->delete();
		$response = [
    		'message' => "Berhasil Log out"
    	];

    	return response($response,200);

	}

    //forgot password
    public function forgot_password(Request $request) {

        $fields = $request->validate([
            'type' => 'required|integer',
            'post' => 'required|string'
        ]);

        if ($request->type == 1) {

            $cek = User::where('user_email',$post)->get();
            if ($cek->isEmpty()) {
                $response = [
                    'type' => $request->type,
                    'post' => $request->post,
                    'message' => 'Maaf Nomor HP tersebut tidak terdaftar'
                ];
                return response($response,500);
            }
            //Send Kode OTP Via WA
            $code = $this->generateRandomString(15,1);
            $update_user = DB::table('t_users')->where('id',$cek[0]->id)->update(['forgot_password_token'=>$code,'forgot_password_time'=>date('Y-m-d H:i:s')]);
            $link    = 'https://cmssouq.trendcas.com/forgot_password/'.base64_encode($cek[0]->id).'/'.$code;
            $message = "Silahkan klik link berikut ini : ".$link.". untuk mengganti password Anda. Waktu Anda hanya 2 jam";

            $send_wa = parent::ZenzifaSms($phone,$code);
            //Send Email
            $substitute = [
                    "Title" => "Perubahan jadwal mentoring",
                    "kelas" => $data_kelas->name,
                    "master" => Auth::guard('admin')->user()->name,
                    "tanggal" => date('d F Y', strtotime($value->date_mentoring)),
                    "jam" => date('H:i', strtotime($value->jam_awal))
            ];
            $sendermail = parent::SendMailFunction($cek_users->email, $cek_users->name,'Perubahan jadwal mentoring',$substitute,'d-3a2fd678625a492db7589f9ff20e90ab');
            // RESPONSE
            $response = [
                'code' => $code,
                'type' => $request->type,
                'post' => $request->post,
                'message' => 'Success'
            ];
            return response($response,200);

        } else {
            $first = substr($request->post, 0, 1);
            if ($first == '0') {
                $phone = $request->code_phone.substr($request->post, 1);
            } else {
                $phone = $request->code_phone.$request->post;
            }
            $cek = User::where('user_phone',$phone)->get();
            if ($cek->isEmpty()) {
                $response = [
                    'type' => $request->type,
                    'post' => $request->post,
                    'message' => 'Maaf nomor WA tersebut tidak terdaftar'
                ];
                return response($response,500);
            }
            //Send Kode OTP Via WA
            $code = $this->generateRandomString(15,1);
            $update_user = DB::table('t_users')->where('id',$cek[0]->id)->update(['forgot_password_token'=>$code,'forgot_password_time'=>date('Y-m-d H:i:s')]);
            $link    = 'https://cmssouq.trendcas.com/forgot_password/'.base64_encode($cek[0]->id).'/'.$code;
            $message = "Silahkan klik link berikut ini : ".$link.". untuk mengganti password Anda. Waktu Anda hanya 2 jam";

            $send_wa = parent::ZenzifaWa($phone,$message);
            // RESPONSE
            $response = [
                'code' => $code,
                'type' => $request->type,
                'post' => $request->post,
                'message' => 'Success'
            ];
            return response($response,200);
        }
    }

    //get_data_profile//
    public function get_data_profile(){
        try {
            $data = User::where('id',auth()->user()->id)->first();
            if($data != null){
                if ($data->profile != null) {
                    $data->profile = env('BASE_IMG').$data->profile;
                }
                $data->city     = parent::GetAddressName('provinces',$data->id_province);
                $data->province = parent::GetAddressName('cities',$data->id_city);
                $data->district = parent::GetAddressName('districts',$data->id_district);
            }
            $response = [
                'data' =>$data,
                'message' => "Get Data"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'AuthController@get_data_profile');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // update profile
    public function update_profile(Request $request) {
        try {
            $input = $request->except('_token');
            if($request->hasfile('profile'))
            {
                $input['profile'] = parent::uploadFileS3($request->file('profile'));
            }
            $update = User::where('id',auth()->user()->id)->update($input);
            $response = [
                'message' => "Berhasil Update Data"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'AuthController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //upload_images
    public function upload_images(Request $request) {

        try {
            $fields = $request->validate([
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:5048'
            ]);
            if($request->hasfile('image'))
            {
                $image = parent::uploadFileS3($request->file('image'));
            }
            $response = [
                'name' => $image,
                'url' => env('BASE_IMG').$image
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'AuthController@upload_images');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    //update phone
    public function update_phone(Request $request) {
        try {

            $fields = $request->validate([
                'type' => 'required|integer',
                'post' => 'required|string'
            ]);
            $first = substr($request->post, 0, 1);
            if ($first == '0') {
                $phone = $request->code_phone.substr($request->post, 1);
            } else {
                $phone = $request->code_phone.$request->post;
            }
            $cek = DB::table('t_users')->where('user_phone',$phone)->whereNull('deleted_at')->get();
            if ($cek->isNotEmpty()) {
                return response([
                    'message' => 'Nomor Tersebut Sudah Ada yang menggunakan',
                ], 500);
            } else {
                $cek2 = DB::table('t_reset_phone')->where('id_user',auth()->user()->id)->whereDate('created_at',date('Y-m-d'))->select('id','request_otp_number')->get();
                $code = $this->generateRandomString(5,1);
                if ($cek2->isNotEmpty()) {
                    if ($cek2[0]->request_otp_number >= 3) {
                        return response([
                            'message' => 'Request Ganti Nomor HP Maksimal 3 kali dalam sehari',
                        ], 500);
                    }
                    $number = $cek2[0]->request_otp_number + 1;
                    $update_otp = DB::table('t_reset_phone')->update([
                        'phone' => $phone,
                        'id_user' => auth()->user()->id,
                        'otp'  => $code,
                        'otp_request_time'=> date('Y-m-d H:i:s'),
                        'request_otp_number'=>$number,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                }

                $insert_otp = DB::table('t_reset_phone')->insertGetId([
                    'phone' => $phone,
                    'id_user' => auth()->user()->id,
                    'otp'  => $code,
                    'otp_request_time'=> date('Y-m-d H:i:s'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            }
            if ($request->type == 1) {
                $message = "Silahkan Masukkan : ".$code.". untuk melanjutkan proses registrasi Anda";
                $send_wa = parent::ZenzifaSms($phone,$code);
            } else {
                $message = "Masuka Kode Verifikasi: ".$code.". untuk melanjutkan proses registrasi Anda";
                $send_wa = parent::ZenzifaWa($phone,$message);

            }

            $response = [
                'mesage' => 'Silahkan Periksa Handphone Anda',
                'phone' => $phone,
            ];
            parent::LogSystem($phone.' Berhasil Melakukan Request ganti Nomor HP',auth()->user()->id,'Update Nomor HP',1,null);

            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'AuthController@update_phone');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    // send validation otp if update number
    public function otp_validation2(Request $request) {
        try {
            $user = DB::table('t_reset_phone')->where('id_user',auth()->user()->id)->where('phone',$request->phone)->where('otp',$request->otp)->first();
            $data = DB::table('t_reset_phone')->where('id_user',auth()->user()->id)->where('phone',$request->phone)->first();

            if ($data->send_otp_number >= 3) {
                return response([
                    'message' => 'Request OTP Maksimal 3 kali dalam 1 menit',
                ], 500);
            }
            $up_number = $data->send_otp_number + 1;
            $update_otp = DB::table('t_reset_phone')->where('id_user',auth()->user()->id)->update(['send_otp_number'=>$up_number]);
            if ($user != null) {

                date_default_timezone_set("Asia/Jakarta");
                $time = new \DateTime($user->otp_request_time);
                $diff = $time->diff(new \DateTime());
                $minutes = ($diff->days * 24 * 60) +
                           ($diff->h * 60) + $diff->i;
                //dd($minutes);
                if ($minutes > 2){

                    return response(['message' => 'Maaf OTP Anda Sudah Kadaluarsa'],500);
                } else {
                    $valid = DB::table('t_users')->where('id',auth()->user()->id)->update(['user_phone'=>$request->phone]);
                    $response = [
                        'message' => 'Selamat Nomor HP Anda berhasil diupdate'
                    ];
                    return response($response,200);
                }

            } else {
                return response(['message' => 'Maaf OTP Anda Salah'],500);
            }

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }
    }

    public function send_otp2(Request $request) {
        try {
            $code = $this->generateRandomString(5,1);
            $cek = DB::table('t_reset_phone')->where('id_user',auth()->user()->id)->where('phone',$request->phone)->select('id','request_otp_number')->get();
            if ($cek[0]->request_otp_number >= 3) {
                return response([
                    'message' => 'Request Ganti Nomor HP Maksimal 3 kali dalam sehari',
                ], 500);
            }

            $number = $cek[0]->request_otp_number + 1;
            $update_number = DB::table('t_reset_phone')->where('id_user',auth()->user()->id)->where('phone',$request->phone)->update(['request_otp_number'=>$number]);
            $reset_number = DB::table('t_reset_phone')->where('id_user',auth()->user()->id)->where('phone',$request->phone)->update(['send_otp_number'=>0]);

            $response = [
                'phone' => $request->phone,
                'message' => 'Silahkan Masukkan OTP Anda'
            ];

            if ($request->type == 1) {
                $message = "Silahkan Masukkan : ".$code.". untuk melanjutkan proses registrasi Anda";
                $send_sms = parent::ZenzifaSms($request->phone,$code);
            } else {
                $message = "Masukan Kode Verifikasi: ".$code.". untuk melanjutkan proses registrasi Anda";
                $send_wa = parent::ZenzifaWa($request->phone,$message);
            }

            return response($response,200);

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }
    }

    public function add_device(Request $request){
        try {
            $input = $request->except('_token');
            $input['id_user'] = auth()->user()->id;
            $input['type'] = 1;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_devices')->insertGetId($input);
            $response = [
                'message' => 'Berhasil menambah devices',
                'data' => $input
            ];
            return response($response,200);

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }
    }

    public function delete_device(Request $request){
        try {
            $insert = DB::table('t_devices')->where('id',$request->id)->update(['type'=>0]);
            $response = [
                'message' => 'Berhasil menghapus devices'
            ];
            return response($response,200);

        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }
    }

    public function get_device(Request $request){
        try {
            $data = DB::table('t_devices')->where('id_user',auth()->user()->id)->where('type',1)->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('device_name','ilike', "%{$search}%");
                    $query->orWhere('device_id','ilike', "%{$search}%");
                })->orderBy('created_at','desc')->get();
            foreach ($data as $k => $v) {
                $data[$k]->shed_name = parent::GetColoumnValue($v->id_shed,'t_sheds','shed_name');
            }
            $response = [
                'data' => $data
            ];
            return response($response,200);
        } catch (Exception $e) {
            return response([
                'message' => 'Caught exception: '. $e->getMessage() ."\n"
            ],500);
        }
    }

}

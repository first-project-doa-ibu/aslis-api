<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use DB;

class CallbackController extends Controller
{
    // Home Index
    public function paymentCallback()
    {
        date_default_timezone_set('Asia/Jakarta');
        try {
            $getHeaders = apache_request_headers();
            $lancode = $getHeaders['X-CALLBACK-TOKEN'];
            $data = json_decode(file_get_contents('php://input'));
            if ($lancode == 'ff3bf9988a5c5c6ce8734dc758822cc7f04cae3249615fee44bc30211e5cc6a2') {
                $invoice = DB::table('t_transactions')->where('id', $data->external_id)->first();
                if($invoice) {
                    if($data->amount == $invoice->total_of_pay) {
                        if ($data->status == 'PAID') {
                            $invoice->status = 2;
                            $invoice->updated_at = date('Y-m-d H:i:s');
                            $invoice->save();
                        } else {
                            return response([
                                'success' => false,
                                'message' => 'EXPIRED'
                            ]);
                        }
                        
                    } else {
                        return response([
                            'success' => false,
                            'message' => 'Nominal Tidak Sesuai'
                        ]);
                    }
                } else {
                    return response([
                        'success' => false,
                        'message' => 'Invoice Tidak Terdaftar'
                    ]);
                }
            
                return response([
                    'success' => true,
                    'message' => 'Success'
                ]);
            } else {
                
                return response([
                    'success' => false,
                    'message' => 'Anda Jangan Macam Macam'
                ]);
            }

        } catch (\Exception $e) {
            return response([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function paymentCallback_ewallet()
    {
        date_default_timezone_set('Asia/Jakarta');
        try {
            $getHeaders = apache_request_headers();
            $lancode = $getHeaders['X-CALLBACK-TOKEN'];
            $data = json_decode(file_get_contents('php://input'));
            // print_r($data->data->id);
            // exit();
            if ($lancode == 'ff3bf9988a5c5c6ce8734dc758822cc7f04cae3249615fee44bc30211e5cc6a2') {
                $invoice = DB::table('t_transactions')->where('id', $data->data->reference_id)->first();
                if($invoice) {
                    
                    if ($data->data->charge_amount == $invoice->total) {
                        if ($data->data->status == 'SUCCEEDED') {
                            $invoice->status     = 2;
                            $invoice->updated_at = date('Y-m-d H:i:s');
                            $invoice->save();
                            
                        } else {
                            return response([
                                'success' => false,
                                'message' => 'EXPIRED'
                            ]);
                        }
                    } else {
                        return response([
                            'success' => false,
                            'message' => 'Salah Kirim'
                        ]);
                    }
                    
                } else {
                    return response([
                        'success' => false,
                        'message' => 'Invoice Tidak Terdaftar'
                    ]);
                }
            
                return response([
                    'success' => true,
                    'message' => 'Success'
                ]);
            } else {
                
                return response([
                    'success' => false,
                    'message' => 'Anda Jangan Macam Macam'
                ]);
            }

        } catch (\Exception $e) {
            return response([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}

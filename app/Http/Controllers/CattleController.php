<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;

class CattleController extends Controller
{
	// Home Index
    public function index(Request $request) {
    	try {
	        $code = 404;
            $cattle = DB::table('t_cattles')->whereNull('deleted_at')->where('id_user',auth('sanctum')->user()->id);

            if ($request->id_kandang != null) {
                $in_array = DB::table('t_shed_histories')->where('status',1)->where('id_new_shed',$request->id_kandang)->pluck('id_cattle')->toArray();
                $cattle = $cattle->whereIn('id',$in_array);
            }

            if ($request->search != null) {
                $cattle = $cattle->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('cattle_name','ilike', "%{$search}%");
                    $query->orWhere('cattle_id','ilike', "%{$search}%");
                });
            }

            if ($request->type != null) {
                $cattle = $cattle->where('type',$request->type);
            }

            if ($request->is_selling != null) {
                $cattle = $cattle->where('is_selling',$request->is_selling);
            }

            if ($request->gender != null) {
                $cattle = $cattle->where('gender',$request->gender);
            }

            if ($request->weigth_min != null) {
                $cattle = $cattle->where('weigth','>=',$request->weigth_min);
            }

            if ($request->weigth_max != null) {
                $cattle = $cattle->where('weigth','<=',$request->weigth_max);
            }

            if ($request->age_min != null) {
                $date1 = date('Y-m-d');
                $newDate1 = date('Y-m-d', strtotime($date1. ' - '.$request->age_min.' years'));

                $cattle = $cattle->whereDate('birthday','<=',$newDate1);
            }

            if ($request->age_max != null) {
                $date = date('Y-m-d');
                $newDate = date('Y-m-d', strtotime($date. ' - '.$request->age_max.' years'));

                $cattle = $cattle->whereDate('birthday','>=',$newDate);
            }

            if (!isset($request->sorting)) {
                $cattle = $cattle->orderBy('created_at','desc');
            }

            if ($request->sorting == 1) {
                $cattle = $cattle->orderBy('cattle_name','asc');
            }
            if ($request->sorting == 2) {
                $cattle = $cattle->orderBy('created_at','desc');
            }
            if ($request->sorting == 3) {
                $cattle = $cattle->orderBy('created_at','asc');
            }

            $cattle = $cattle->select('id','cattle_name','gender','weigth','birthday','photo','id_category','is_selling','selling_price','discount','final_price');
            $cattle = $cattle->paginate(10);
            if ($cattle->isNotEmpty()) {
                $code = 200;
                foreach ($cattle as $k => $v) {
                    //if ($v->photo != null) {
                    $cattle[$k]->photo = parent::ImageCattle($v->id);
                    if ($v->weigth > 0) {
                        $cattle[$k]->weigth = number_format((float)$v->weigth, 2, '.', '');
                    }
                    //}
                    $cattle[$k]->category = $this->CategoryCattle($v->id_category);
                    $cattle[$k]->age = parent::getAge($v->birthday);
                    $cattle[$k]->kandang = parent::getKandangNow($v->id);
                }
            }

            $response = [
                'code' => $code,
                'data' => $cattle
            ];

            return response($response,200);
    	} catch (\Exception $err) {
    		$error = $err->getMessage();
    		//Insert Log Error
    		parent::LogErrorCreate($error,'CattleController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function CategoryCattle($id){
        $return = "-";
        $data = DB::table('t_cattle_categories')->where('id',$id)->pluck('category_name');
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    //detail//
    public function detail(Request $request){
        try {
            $isUuid = Str::isUuid($request->id);

            if ($isUuid == true) {
                $data = DB::table('t_cattles')->where('id',$request->id)->whereNull('deleted_at')->first();
            } else {
                $data = DB::table('t_cattles')->where('cattle_id',$request->id)->whereNull('deleted_at')->first();
            }

            if($data != null){
                $data->is_editable = 0;
                if ($data->id_user == auth()->user()->id) {
                    $data->is_editable = 1;
                }
                $data->is_edit_weigth       = $this->CattleCheckStatus($data->id,'t_weigth_histories');
                $data->is_edit_kandang      = $this->CattleCheckStatus($data->id,'t_shed_histories');
                $data->photo                = parent::BaseFile($data->id);
                $data->male_parent_name     = $this->CattleParent($data->male_parent,'cattle_name');
                $data->female_parent_name   = $this->CattleParent($data->female_parent,'cattle_name');
                $data->male_parent_id       = $this->CattleParent($data->male_parent,'cattle_id');
                $data->female_parent_id     = $this->CattleParent($data->female_parent,'cattle_id');
                $data->category_name        = $this->CategoryCattle($data->id_category);
                $data->total_food           = parent::TFoodCattle($data->id,'weigth');
                $data->total_fee_food       = parent::TFoodCattle($data->id,'price_food');
                $data->total_growth_weigth  = parent::TGrowthWeigth($data->id,$data->weigth);
                $data->id_kandang           = $this->ShedCattle($data->id,'id');
                $data->nama_kandang         = $this->ShedCattle($data->id,'shed_name');
                $data->age                  = parent::getAge($data->birthday);
                $data->weigth_graphic       = $this->weigth_graphic($data->id);

                $data->weight_history = $this->HistoryWeigth($data->id);
                $data->eat_history = $this->HistoryEat($data->id);
                $data->kandang     = parent::getKandangNow($data->id);
                $data->id_problem  = "ca17e86b-393b-46de-b880-b0680333056a";
                $data->date_last_weigth = $this->LastWeigth($data->id);
                $response = [
                    'data' =>$data,
                    'message' => "Get Data"
                ];

                return response($response,200);
            } else {
                return response([
                    'message' => 'Maaf ID tidak ditemukan',
                ], 500);
            }

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@detail');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function weigth_graphic($id){
        $return = [];
        $data = DB::table('t_weigth_histories')->where('id_cattle',$id)->select('date','weigth')->orderBy('date','asc')->get();
        if ($data->isNotEmpty()) {
            $return = $data;
        }
        return $return;
    }

    public function ShedCattle($id,$coloum){
        $return = "";
        $data = DB::table('t_shed_histories')->where('id_cattle',$id)->where('status',1)->pluck('id_new_shed');
        $data_shed = DB::table('t_sheds')->where('id',$data[0])->pluck($coloum);
        if ($data_shed->isNotEmpty()) {
            $return = $data_shed[0];
        }
        return $return;
    }

    public function CattleCheckStatus($id,$table){
        $return = 1;
        $data = DB::table($table)->where('id_cattle',$id)->where('is_new',0)->get();
        if ($data->isNotEmpty()) {
            $return = 0;
        }
        return $return;
    }

    public function CattleParent($id,$coloum){
        $return = " - ";
        $data = DB::table('t_cattles')->where('id',$id)->pluck($coloum);
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    public function HistoryWeigth($id){
        $return = null;
        $data = DB::table('t_weigth_histories')->where('id_cattle',$id)->select('weigth','created_at','photo','date','id_cattle')->orderBy('date','asc')->limit(5)->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                if ($v->photo != null) {
                    $data[$k]->photo = 'https://api-aslis.angkitagro.com/'.$v->photo;
                } else {
                    $data[$k]->photo = parent::ImageCattle($v->id_cattle);
                }
            }
            $return = $data;
        }
        return $return;
    }

    public function HistoryEat($id){
        $return = null;
        $data = DB::table('t_eat_histories as h')->join('t_foods as f','f.id','h.id_cattle')->join('t_sheds as s','s.id','h.id_shed')->where('id_cattle',$id)->select('h.weigth','h.created_at','food_name','shed_name','f.price','h.date','h.id_cattle')->orderBy('h.created_at','desc')->limit(5)->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {

                if ($v->photo != null) {
                    $data[$k]->photo = env('BASE_IMG').$v->photo;
                } else {
                    $data[$k]->photo = parent::ImageCattle($v->id_cattle);
                }

                if ($v->price > 0) {
                    $data[$k]->price = number_format((float)$v->price, 0, '.', '');
                }
            }
            $return = $data;
        }
        return $return;
    }

    // tambah ternak
    public function add(Request $request) {
        try {
            $input = $request->except('_token','id_shed','photo1','photo2','photo3','photo4','photo5','video');
            //cek id shame
            if ($request->shed_id != '' || $request->shed_id != null) {
                $cek_id = DB::table('t_cattles')->whereNull('deleted_at')->where('shed_id',$request->shed_id)->get();
                if ($cek_id->isNotEmpty()) {
                    return response([
                        'message' => 'Maaf ID Tersebut sudah ada di sistem',
                    ], 500);
                }

            }
            //cek kapasitas kandang
            $shed = DB::table('t_sheds')->where('id',$request->id_shed)->first();
            if ($shed != null) {
                $cek_kapasitas = DB::table('t_shed_histories')->where('id_new_shed',$request->id_shed)->where('status',1)->count();
                if ($shed->capacity <= $cek_kapasitas) {
                    return response([
                        'message' => 'Maaf kapasitas kandang sudah penuh',
                    ], 500);
                }
            } else {
                return response([
                    'message' => 'Maaf kandang Tidak ditemukan',
                ], 500);
            }


            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['input_date'] = date('Y-m-d');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_cattles')->insertGetId($input);
            //input photos and videos
            if($request->hasfile('photo1'))
            {
                $photo1 = parent::uploadFileS3($request->file('photo1'));
                $input_photo1 = DB::table('t_aslis_files')->insert(['id_object'=>$insert,'type'=>1,'file'=>$photo1, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('photo2'))
            {
                $photo2 = parent::uploadFileS3($request->file('photo2'));
                $input_photo2 = DB::table('t_aslis_files')->insert(['id_object'=>$insert,'type'=>1,'file'=>$photo2, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('photo3'))
            {
                $photo3 = parent::uploadFileS3($request->file('photo3'));
                $input_photo3 = DB::table('t_aslis_files')->insert(['id_object'=>$insert,'type'=>1,'file'=>$photo3, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('photo4'))
            {
                $photo4 = parent::uploadFileS3($request->file('photo4'));
                $input_photo4 = DB::table('t_aslis_files')->insert(['id_object'=>$insert,'type'=>1,'file'=>$photo4, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('video'))
            {
                $video = parent::uploadFileS3($request->file('video'));
                $input_video = DB::table('t_aslis_files')->insert(['id_object'=>$insert,'type'=>2,'file'=>$video, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            //end photo
            $input_history_kandang = DB::table('t_shed_histories')->insert(['id_cattle'=>$insert,'date'=>date('Y-m-d'),'id_new_shed'=>$request->id_shed, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'description'=>'Kambing baru masuk Kandang','is_new'=>1]);
            //t_weigth_histories
            $input_history_berat = DB::table('t_weigth_histories')->insert(['id_cattle'=>$insert,'date'=>date('Y-m-d'),'weigth'=>$request->weigth, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'id_shed'=>$request->id_shed,'is_new'=>1]);

            $response = [
                'data' => $input,
                'message' => "Berhasil menambah ternak"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // update ternak
    public function update(Request $request) {
        try {
            $input = $request->except('_token','id_shed','photo1','photo2','photo3','photo4','photo5','video');
            $existing = DB::table('t_cattles')->where('id',$request->id)->first();

            //cek id shame
            if ($request->shed_id != '' || $request->shed_id != null) {
                $cek_id = DB::table('t_cattles')->whereNull('deleted_at')->where('shed_id',$request->shed_id)->where('id','!=',$request->id)->get();
                if ($cek_id->isNotEmpty()) {
                    return response([
                        'message' => 'Maaf ID Tersebut sudah ada di sistem',
                    ], 500);
                }

            }
            //cek kapasitas kandang
            $shed = DB::table('t_sheds')->where('id',$request->id_shed)->first();
            if ($shed != null) {
                $cek_kapasitas = DB::table('t_shed_histories')->where('id_new_shed',$request->id_shed)->where('status',1)->count();
                if ($shed->capacity <= $cek_kapasitas) {
                    return response([
                        'message' => 'Maaf kapasitas kandang sudah penuh',
                    ], 500);
                }
            } else {
                return response([
                    'message' => 'Maaf kandang Tidak ditemukan',
                ], 500);
            }
            // $photo = $existing->photo;
            // if($request->hasfile('photo'))
            // {
            //     $photo = parent::uploadFileS3($request->file('photo'));
            // }
            // $input['photo'] = $photo;
            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_cattles')->where('id',$request->id)->update($input);
            //input photos and videos
            if($request->hasfile('photo1'))
            {
                $photo1 = parent::uploadFileS3($request->file('photo1'));
                $input_photo1 = DB::table('t_aslis_files')->insert(['id_object'=>$request->id,'type'=>1,'file'=>$photo1, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('photo2'))
            {
                $photo2 = parent::uploadFileS3($request->file('photo2'));
                $input_photo2 = DB::table('t_aslis_files')->insert(['id_object'=>$request->id,'type'=>1,'file'=>$photo2, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('photo3'))
            {
                $photo3 = parent::uploadFileS3($request->file('photo3'));
                $input_photo3 = DB::table('t_aslis_files')->insert(['id_object'=>$request->id,'type'=>1,'file'=>$photo3, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('photo4'))
            {
                $photo4 = parent::uploadFileS3($request->file('photo4'));
                $input_photo4 = DB::table('t_aslis_files')->insert(['id_object'=>$request->id,'type'=>1,'file'=>$photo4, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            if($request->hasfile('video'))
            {
                $video = parent::uploadFileS3($request->file('video'));
                $input_video = DB::table('t_aslis_files')->insert(['id_object'=>$request->id,'type'=>2,'file'=>$video, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            }
            //end photo
            $input_history_kandang = DB::table('t_shed_histories')->where('id_cattle',$request->id)->where('is_new',1)->update(['date'=>date('Y-m-d'),'id_new_shed'=>$request->id_shed, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'description'=>'Kambing baru yang diedit']);
            //t_weigth_histories
            $input_history_berat = DB::table('t_weigth_histories')->where('id_cattle',$request->id)->where('is_new',1)->update(['date'=>date('Y-m-d'),'weigth'=>$request->weigth, 'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'id_shed'=>$request->id_shed]);

            $response = [
                'data' => $existing,
                'message' => "Berhasil mengedit ternak"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // delete ternak
    public function delete(Request $request) {
        try {

            $insert = DB::table('t_cattles')->where('id',$request->id)->update(['status'=>0,'deleted_at'=>date('Y-m-d')]);

            $response = [
                'message' => "Berhasil menghapus ternak"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@delete');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // set status ternak
    public function set_status(Request $request) {
        try {

            $insert = DB::table('t_cattles')->where('id',$request->id)->update(['status'=>$request->status,'updated_at'=>date('Y-m-d H:i:s')]);

            $response = [
                'message' => "Berhasil menghapus ternak"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@set_status');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //search
    public function search(Request $request) {
        try {
            $code = 404;
            $cattle = [];
            $cattle = DB::table('t_cattles')->where('status',1)->where('id_user',auth('sanctum')->user()->id);
            if ($request->id_kandang != null) {
                $in_array = DB::table('t_shed_histories')->where('status',1)->where('id_new_shed',$request->id_kandang)->pluck('id_cattle')->toArray();
                $cattle = $cattle->whereIn('id',$in_array);
            }

            if ($request->search != null) {
                $cattle = $cattle->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('cattle_name','ilike', "%{$search}%");
                    $query->orWhere('cattle_id','ilike', "%{$search}%");
                });
            }
            $cattle = $cattle->select('id','cattle_name','cattle_id','photo','weigth')->get();
            if ($cattle->isNotEmpty()) {
                $code = 200;
                foreach ($cattle as $k => $v) {
                    if ($v->photo != null) {
                        $cattle[$k]->photo = parent::ImageCattle($v->id);
                    }
                    $cattle[$k]->date_off_weigth = $this->LastWeigth($v->id);
                }
            }

            $response = [
                'code' => $code,
                'data' => $cattle
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@search');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function LastWeigth($id){
        $return = 'Belum Pernah ditimbang';
        $data = DB::table('t_weigth_histories')->where('id_cattle',$id)->orderBy('date','desc')->pluck('date');
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    //move_shad
    public function move_shad(Request $request) {
        try {

            $input = $request->except('_token','id_shed');

            $shed = DB::table('t_sheds')->where('id',$request->id_new_shed)->first();
            if ($shed != null) {
                $cek_kapasitas = DB::table('t_shed_histories')->where('id_new_shed',$request->id_new_shed)->where('status',1)->count();
                if ($shed->capacity <= $cek_kapasitas) {
                    return response([
                        'message' => 'Maaf kapasitas kandang sudah penuh',
                    ], 500);
                }
            } else {
                return response([
                    'message' => 'Maaf kandang Tidak ditemukan',
                ], 500);
            }

            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_shed_histories')->insertGetId($input);

            $response = [
                'data' => $input,
                'message' => "Berhasil memindahkan ternak"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@move_shad');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //give_food
    public function give_food(Request $request) {
        try {
            $in_shed = DB::table('t_shed_histories as s')->join('t_cattles as c','c.id','s.id_cattle')->where('c.status',1)->where('s.status',1)->where('id_new_shed',$request->id_shed)->pluck('id_cattle');
            if ($in_shed->isEmpty()) {
                return response(['message'=>'Maaf kandang tersebut sudah kosong'],500);
            } else {
                //cek ketersediaan pakan
                $cek = DB::table('t_foods')->where('id',$request->id_food)->select('capital_price','available_stock','stock')->get();
                if ($request->weigth <= $cek[0]->available_stock) {
                    //insert history
                    $inputHistory = ['id_shed'=>$request->id_shed,
                                  'id_food'=>$request->id_food,
                                  'id_user'=>auth('sanctum')->user()->id,
                                  'date'=>$request->date,
                                  'weigth'=>$request->weigth,
                                  'created_at'=>date('Y-m-d H:i:s'),
                                  'updated_at'=>date('Y-m-d H:i:s')
                                 ];
                    $id_history = DB::table('t_eat_per_shed')->insertGetId($inputHistory);

                    $cattle = DB::table('t_cattles')->whereIn('id',$in_shed->toArray())->get();
                    foreach ($cattle as $k => $v) {
                        $price_in  = $cek[0]->capital_price / $cek[0]->stock;
                        $weigth = $request->weigth / count($cattle);
                        $price = $price_in * $weigth;
                        //histori makan
                        $input  = ['id_shed'=>$request->id_shed,
                                  'id_food'=>$request->id_food,
                                  'id_cattle'=>$v->id,
                                  'id_history'=>$id_history,
                                  'price_food'=>$price,
                                  'date'=>$request->date,
                                  'weigth'=>$weigth,
                                  'created_at'=>date('Y-m-d H:i:s'),
                                  'updated_at'=>date('Y-m-d H:i:s')
                                 ];
                        $insert = DB::table('t_eat_histories')->insertGetId($input);
                        $response = [
                            'message' => "Berhasil memberi makan ternak"
                        ];
                    }
                    // kurangi stock pakan
                    $stock_now = $cek[0]->available_stock - $request->weigth;
                    $update_stock = DB::table('t_foods')->where('id',$request->id_food)->update(['available_stock'=>$stock_now]);
                } else {
                    return response(['message'=>'Maaf stock hanya tersedia sebesar '.$cek[0]->available_stock],500);
                }

            }

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@give_food');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    //weigth_noted
    public function weigth_noted(Request $request) {
        try {
            $data = json_decode(request()->getContent(), true);
            if (isset($data['catatan'])) {
                $catatan = $data['catatan'];
                if (count($catatan) > 0) {
                    $no = 0;
                    $inputHistory = ['id_shed'=>$data['id_shed'],
                                  'id_user'=>auth('sanctum')->user()->id,
                                  'date'=>$data['date'],
                                  'created_at'=>date('Y-m-d H:i:s'),
                                  'updated_at'=>date('Y-m-d H:i:s')
                                 ];
                    $id_history = DB::table('t_weigth_per_shed')->insertGetId($inputHistory);
                    foreach ($catatan as $k => $v) {
                    $no = $no + 1;

                        //update berat hewan
                        if ($catatan[$k]['weigth'] != null || $catatan[$k]['weigth'] != '') {
                            $data_catatan = ['id_cattle'=>$catatan[$k]['id_cattle'],
                                'id_shed'=>$data['id_shed'],
                                'id_history'=>$id_history,
                                'date'=>$data['date'],
                                'weigth'=>$catatan[$k]['weigth'],
                                'photo'=>$catatan[$k]['photo'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];

                            $insert = DB::table('t_weigth_histories')->insertGetId($data_catatan);
                            DB::table('t_cattles')->where('id',$catatan[$k]['id_cattle'])->update(['weigth'=>$catatan[$k]['weigth']]);
                        }
                    }

                } else {
                    return response(['message'=>'Catatan berat wajid diisi'],500);
                }
            } else {
                return response(['message'=>'Catatan berat wajid diisi'],500);
            }
            $response = [
                            'message' => "Berhasil mencatat berat ternak"
                        ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@weigth_noted');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function weigth_noted_from_scan(Request $request){
        try {
            //dd("dancook");
            $inputHistory = ['id_shed'=>$request->id_shed,
                              'id_user'=>auth('sanctum')->user()->id,
                              'date'=>$request->date,
                              'created_at'=>date('Y-m-d H:i:s'),
                              'updated_at'=>date('Y-m-d H:i:s')
                             ];
            $id_history = DB::table('t_weigth_per_shed')->insertGetId($inputHistory);
            $data = ['id_cattle'=>$request->id_cattle,'id_shed'=>$request->id_shed,'date'=>$request->date,'weigth'=>$request->weigth,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'id_history'=>$id_history];
            $insert = DB::table('t_weigth_histories')->insert($data);
            $update = DB::table('t_cattles')->where('id',$request->id_cattle)->update(['weigth'=>$request->weigth]);
            $response = [
                            'message' => "Berhasil mencatat berat ternak"
                        ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@weigth_noted_from_scan');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //weigth_history
    public function weigth_history(Request $request) {
        try {
            $code = 404;
            $cattle = DB::table('t_weigth_histories as h')->join('t_sheds as s','s.id','h.id_shed')->join('t_cattles as c','c.id','h.id_cattle')->where('c.id_user',auth()->user()->id);
//            dd(DB::table('t_cattles')->where('id_user',auth()->user()->id)->paginate(10));
            if ($request->id_cattle != null) {
                $cattle = $cattle->where('h.id_cattle',$request->id_cattle);
            }

            if ($request->id_shed != null) {
                $cattle = $cattle->where('h.id_shed',$request->id_shed);
            }

            if ($request->start_date != null) {
                $cattle = $cattle->whereDate('h.date','>=',$request->start_date);
            }

            if ($request->end_date != null) {
                $cattle = $cattle->whereDate('h.date','<=',$request->end_date);
            }

            if ($request->search != null) {
                $cattle = $cattle->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->orWhere('shed_name','ilike', "%{$search}%");
                    $query->orWhere('shed_id','ilike', "%{$search}%");
                    $query->orWhere('cattle_name','ilike', "%{$search}%");
                    $query->orWhere('cattle_id','ilike', "%{$search}%");
                });
            }

            $cattle = $cattle->select('h.id','h.photo','h.created_at','h.weigth','shed_name','cattle_name','h.date','h.id_cattle');
            if (!isset($request->sorting)) {
                $cattle = $cattle->orderBy('h.date','asc');
            }

            if ($request->sorting == 1) {
                $cattle = $cattle->orderBy('shed_name','asc');
            }
            if ($request->sorting == 2) {
                $cattle = $cattle->orderBy('h.created_at','desc');
            }
            if ($request->sorting == 3) {
                $cattle = $cattle->orderBy('h.created_at','asc');
            }
            $cattle = $cattle->paginate(10);
            if ($cattle->isNotEmpty()) {
                $code = 200;
                foreach ($cattle as $k => $v) {
                    if ($v->photo == null) {
                        $cattle[$k]->photo = parent::ImageCattle($v->id_cattle);
                    } else {
                        $cattle[$k]->photo = 'https://api-aslis.angkitagro.com/'.$v->photo;
                    }
                    $cattle[$k]->date_off_weigth = $this->LastWeigth($v->id_cattle);
                }
            }

            $response = [
                'code' => $code,
                'data' => $cattle
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@weigth_history');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //weigth_history
    public function weigth_history_shed(Request $request) {
        try {
            $code = 404;
            $cattle = DB::table('t_weigth_per_shed as h')->whereNull('h.deleted_at')->join('t_sheds as s','s.id','h.id_shed')->where('s.id_user',auth('sanctum')->user()->id);

            if ($request->id_shed != null) {
                $cattle = $cattle->where('h.id_shed',$request->id_shed);
            }

            if ($request->start_date != null) {
                $cattle = $cattle->where('h.date','>=',$request->start_date);
            }

            if ($request->end_date != null) {
                $cattle = $cattle->whereDate('h.date','<=',$request->end_date);
            }

            if ($request->search != null) {
                $cattle = $cattle->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->orWhere('shed_name','ilike', "%{$search}%");
                    $query->orWhere('shed_id','ilike', "%{$search}%");
                });
            }

            $cattle = $cattle->select('h.id','h.created_at','h.date','shed_name', 'photo');
            if (!isset($request->sorting)) {
                $cattle = $cattle->orderBy('h.date','desc');
            }

            if ($request->sorting == 1) {
                $cattle = $cattle->orderBy('shed_name','asc');
            }
            if ($request->sorting == 2) {
                $cattle = $cattle->orderBy('h.created_at','desc');
            }
            if ($request->sorting == 3) {
                $cattle = $cattle->orderBy('h.created_at','desc');
            }
            $cattle = $cattle->paginate(10);
            if ($cattle->isNotEmpty()) {
                $code = 200;
                foreach ($cattle as $k => $v) {
                    if ($v->photo != null) {
                        $cattle[$k]->photo = parent::ImageCattle($v->id_cattle);
                    } else {
                        $cattle[$k]->photo = env('BASE_IMG').$v->photo;
                    }

                    $cattle[$k]->total_weigth = $this->weigthOfSheds($v->id);
                }
            }

            $response = [
                'code' => $code,
                'data' => $cattle
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@weigth_history');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function weigthOfSheds($id){
        $sum = DB::table('t_weigth_histories')->where('id_history',$id)->sum('weigth');
        if ($sum > 0) {
            $return = number_format((float)$sum, 2, '.', '');
        } else {
            $return = "0";

        }
        return (string)$return;
    }

    // move shed
    public function move_shed(Request $request) {
        try {

            $input = $request->except('_token');
            $shed = DB::table('t_sheds')->where('id',$request->id_new_shed)->first();
            if ($shed != null) {
                $cek_kapasitas = DB::table('t_shed_histories')->where('id_new_shed',$request->id_new_shed)->where('status',1)->count();
                if ($shed->capacity <= $cek_kapasitas) {
                    return response([
                        'message' => 'Maaf kapasitas kandang sudah penuh',
                    ], 500);
                }
            } else {
                return response([
                    'message' => 'Maaf kandang Tidak ditemukan',
                ], 500);
            }
            //set staus 0;
            $update_status = DB::table('t_shed_histories')->where('id_cattle',$request->id_cattle)->update(['status'=>0]);

            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_shed_histories')->insertGetId($input);

            $response = [
                'message' => "Berhasil memindahkan ternak"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@move_shed');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //detail_weigth_history
    public function detail_weigth_history(Request $request){
        try {
            $data = DB::table('t_weigth_per_shed')->where('id',$request->id)->first();
            if($data != null){
                $data->catatan  = $this->CatatanWeigth($data->id);
            }
            $response = [
                'data' =>$data,
                'message' => "Get Data"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@detail');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function CatatanWeigth($id){
        $return = [];
        $cattle = DB::table('t_weigth_histories as h')->join('t_cattles as c','c.id','h.id_cattle')->where('id_history',$id)->select('cattle_name','cattle_id','h.photo','h.weigth','id_cattle')->get();
        if ($cattle->isNotEmpty()) {
            foreach ($cattle as $k => $v) {
                if ($v->photo != null) {
                    $cattle[$k]->url_photo = 'https://api-aslis.angkitagro.com/'.$v->photo;
                } else {
                    $cattle[$k]->url_photo = parent::ImageCattle($v->id_cattle);
                }
                $cattle[$k]->date_off_weigth = $this->LastWeigth($v->id_cattle);
            }
            $return = $cattle;
        }
        return $return;
    }

    // delete history berat
    public function delete_weigth_history(Request $request) {
        try {

            $insert = DB::table('t_weigth_per_shed')->where('id',$request->id)->update(['deleted_at'=>date('Y-m-d')]);
            $delete = DB::table('t_weigth_histories')->where('id_history',$request->id)->delete();

            $response = [
                'message' => "Berhasil menghapus history berat"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@delete_weigth_history');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //update weigth history
    public function update_weigth_history(Request $request) {
        try {
            $data = json_decode(request()->getContent(), true);
            if (isset($data['catatan'])) {
                $catatan = $data['catatan'];
                if (count($catatan) > 0) {
                    $no = 0;
                    $inputHistory = ['date'=>$data['date'],
                                  'updated_at'=>date('Y-m-d H:i:s')
                                 ];
                    $id_history = DB::table('t_weigth_per_shed')->where('id',$request->id)->update($inputHistory);
                    DB::table('t_weigth_histories')->where('id_history',$request->id)->delete();
                    foreach ($catatan as $k => $v) {
                    $no = $no + 1;
                        //insert catatan
                        //update berat hewan
                        if ($catatan[$k]['weigth'] != null || $catatan[$k]['weigth'] != '') {
                            $data_catatan = ['id_cattle'=>$catatan[$k]['id_cattle'],
                                'id_shed'=>$data['id_shed'],
                                'id_history'=>$request->id,
                                'date'=>$data['date'],
                                'weigth'=>$catatan[$k]['weigth'],
                                'photo'=>$catatan[$k]['photo'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];

                            $insert = DB::table('t_weigth_histories')->insertGetId($data_catatan);

                            DB::table('t_cattles')->where('id',$catatan[$k]['id_cattle'])->update(['weigth'=>$catatan[$k]['weigth']]);
                        }
                    }

                } else {
                    return response(['message'=>'Catatan berat wajid diisi'],500);
                }
            } else {
                return response(['message'=>'Catatan berat wajid diisi'],500);
            }
            $response = [
                            'message' => "Berhasil update berat ternak"
                        ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@weigth_noted');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    //public function delete_file
    public function delete_file(Request $request){
        try {
            $delete = DB::table('t_aslis_files')->where('id',$request->id)->update(['deleted_at'=>date('Y-m-d')]);
            $response = [
                            'message' => "Berhasil menghapus foto/video ternak"
                        ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@weigth_noted');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }

    }

    public function sale(Request $request){
        try {
            $last_price = $request->selling_price;
            if (isset($request->discount)) {
                if ($request->discount != 0 || $request->discount != '') {
                    $diskon = ($request->selling_price * $request->discount) / 100;
                    $last_price = (float)$request->selling_price - $diskon;
                }
            }
            $update = DB::table('t_cattles')->where('id',$request->id)->update(['is_selling'=>1,'updated_at'=>date('Y-m-d'),'selling_price'=>$request->selling_price,'discount'=>$request->discount,'final_price'=>$last_price]);
            $response = [
                            'message' => "Berhasil menjual ternak Anda"
                        ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@sale');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function unsale(Request $request){
        try {
            $update = DB::table('t_cattles')->where('id',$request->id)->update(['is_selling'=>0,'updated_at'=>date('Y-m-d'),'selling_price'=>0]);
            $response = [
                            'message' => "Berhasil membatalkan penjualan ternak Anda"
                        ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@sale');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\LogUsersModel;
use App\Models\ErrorModel;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Classes\S3;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use DB;
use App\Models\Product;
use App\Models\Shops;
use App\Models\User;
use App\Models\WareHouse;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function LogErrorCreate($message,$controller) {
	    date_default_timezone_set("Asia/Jakarta");
	    $id_o = null;
	    if (isset(auth()->user()->id)) {
	        $id_o = auth()->user()->id;
	    }
	    $data_insert = array(
	        'exception' => $message,
	        'line_error' => 0,
	        'controller' => $controller,
	        'id_user'    => $id_o,
	        'tipe_user' => 2,
	        'failed_at' => date('Y-m-d H:i:s')
	    );
	    $insert = ErrorModel::insertGetId($data_insert);
	    return $insert;
	}

	public function LogSystem($message,$id,$menu,$type,$id_detail) {
	    date_default_timezone_set("Asia/Jakarta");
	    $data_insert = array(
	        'text_logs' => $message,
	        'id_detail_object' => $id_detail,
	        'date' => date('Y-m-d'),
	        'menu' => $menu,
	        'id_user' => $id,
	        'type' => $type,
	        'created_at' => date('Y-m-d H:i:s'),
	        'updated_at' => date('Y-m-d H:i:s')
	    );
	    $insert = LogUsersModel::insertGetId($data_insert);
	    return $insert;
	}

	public function cleanHazard($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public function uploadFileS3($file){
        $acces_key = env('AWS_ACCESS_KEY_ID');
        //dd($acces_key);
        $secret_key = env('AWS_SECRET_ACCESS_KEY');
        $s3 = new S3($acces_key,$secret_key);
        $bucket = env('AWS_BUCKET');
        //$image = $file->hashName();
        $name = $this->cleanHazard(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
        //dd($name);
        $image = $name.'_'.time().'.'.$file->getClientOriginalExtension();

        $file_path = public_path('images/'.$image);
        $file->move('images/', $image);
        $s3->putObjectFile($file_path, $bucket, $image, S3::ACL_PUBLIC_READ);
        File::delete($file_path);

        return $image;
    }

    public function ZenzifaWa($phone,$message){
        //Send Kode OTP Via WA
        $userkey=env('USERKEY_ZENZIFA');// userkey lihat di zenziva
        $passkey=env('PASSKEY_ZENZIFA');
        $url = "https://gsm.zenziva.net/api/sendWA/";
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS,
        'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$phone.'&pesan='.$message);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        $results = curl_exec($curlHandle);

        return $results;
    }

    public function ZenzifaSms_old($phone,$message)
    {

        $userkey="45o2qa"; // userkey lihat di zenziva
        $passkey="461e85b874184290030aaa72";

        $url = "https://gsm.zenziva.net/api/sendsms/";
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS,
        'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$phone.'&pesan='.$message);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        $results = curl_exec($curlHandle);
        // UPDATE TOKEN
        return $results;

    }

    public function ZenzifaSms($phone,$otp){
        $userkey = '45o2qa';
        $passkey = '461e85b874184290030aaa72';
        //$telepon = '081111111111';
        //$otp = '743649';
        $url = 'https://gsm.zenziva.net/api/sendOTP/';
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
            'userkey' => $userkey,
            'passkey' => $passkey,
            'nohp' => $phone,
            'kode_otp' => $otp
        ));
        $results = json_decode(curl_exec($curlHandle), true);
        curl_close($curlHandle);
    }

    public function SendMailFunction($i_email, $i_name,$title,$substitute,$template_id){
        $email = new Mail();
        $email->setFrom("mastermentor@gmail.com", "MASTER MENTOR");
        $email->setSubject($title);
        $email->addTo($i_email, $i_name);
        $email->setTemplateId($template_id);

        $substitutions = $substitute;
        $email->addDynamicTemplateDatas($substitutions);
        $sendgrid = new \SendGrid(env('SENDGRID_CODE'));
        try {
            $response = $sendgrid->send($email);

            // dd($response);
            $code = $response->statusCode();
        } catch (Exception $e) {
            //echo 'Caught exception: '.  $e->getMessage(). "\n";
            $code = 500;
        }
        return $code;
    }

    public function ResponseError($message,$code){
        return response([
            'message' => $message
        ],$code);
    }

    public function generateRandomString($length,$type) {
        if ($type == 1) {
            $characters = '0123456789';
        } else {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    // get category by id
    public function GetCaegoryProduct($id){
        $return = "";
        $data = DB::table('relation_product_category as r')->join('t_category_products as c','c.id','r.id_category')->where('id_product',$id)->pluck('category_product_name');
        if ($data->isNotEmpty()) {
            $return = implode (", ", $data->toArray());
        }
        return $return;
    }

    // get category by id
    public function GetImageProduct($id){
        $return = "";
        $data = DB::table('t_product_images')->where('id_product',$id)->whereNull('deleted_at')->where('is_thumbnail',1)->pluck('image');
        if ($data->isNotEmpty()) {
            if ($data[0] != null) {
                $return = env('BASE_IMG').$data[0];
            } else {
                $group = DB::table('t_products')->where('id',$id)->pluck('group_product');
                $main_product = DB::table('t_products')->where('group_product',$group[0])->where('main_product',1)->pluck('id');
                $image = DB::table('t_product_images')->where('id_product',$main_product[0])->whereNull('deleted_at')->where('is_thumbnail',1)->pluck('image');
                if ($image[0] != null) {
                    $return = env('BASE_IMG').$image[0];
                } else {
                    $return = null;
                }
            }

        }
        return $return;
    }

    //
    public function GetImageProductWithId($id){
        $data = [];
        $data = DB::table('t_product_images')->where('id_product',$id)->whereNull('deleted_at')->where('is_thumbnail',1)->select('image')->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                $data[$k]->link_image = env('BASE_IMG').$v->image;
            }

        }
        return $data;
    }

    // get All Image Product
    public function AllImageProduct($id){
        $return = [];
        $data = DB::table('t_product_images')->where('id_product',$id)->whereNull('deleted_at')->orderBy('is_thumbnail','desc')->select('image')->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                $return[] = env('BASE_IMG').$v->image;
            }
        }
        return $return;
    }

    // get All Image Product
    public function AllImageProductWithId($id){
        $data = [];
        $data = DB::table('t_product_images')->where('id_product',$id)->whereNull('deleted_at')->orderBy('is_thumbnail','desc')->select('image')->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                $data[$k]->link_image = env('BASE_IMG').$v->image;
            }
        }
        return $data;
    }

    public function NumberReviewProduct($id){

        $data = DB::table('t_review_products')->where('id_product',$id)->count();
        return $data;

    }

    public function NumberCartProduct($id){

        $data = DB::table('t_carts')->where('id_product',$id)->where('status',1)->distinct('id_user')->count();
        return $data;

    }

    public function GetAddressName($table,$id){
        $return = "";
        $data = DB::table($table)->where('id',$id)->pluck('name');
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    //Insert system untuk dikelola data base
    public function insertSystem($tipe,$id_item,$id_user,$ip_device,$device_brand){
        date_default_timezone_set("Asia/Jakarta");
        $data_insert = array(
            'id_user' => $id_user,
            'id_object' => $id_item,
            'ip_device' => $ip_device,
            'device_brand' => $device_brand,
            'type' => $tipe,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        );
        $insert = DB::table('t_viewer_souqs')->insertGetId($data_insert);
    }

    // get data variant product
    public function VariantProduct($group){
        $return['model'] = [];
        $return['color'] = [];
        $variant_model = DB::table('t_products')->where('group_product',$group)->where('main_product',0)->whereNotNull('variant_model')->where('variant_model','!=','')->select('variant_model')->distinct('variant_model')->get();
        if ($variant_model->isNotEmpty()) {
            foreach ($variant_model as $k => $v) {
                $return['model'][] = $v->variant_model;
            }
        }
        $variant_color = DB::table('t_products')->where('group_product',$group)->where('main_product',0)->whereNotNull('variant_color')->where('variant_color','!=','')->select('variant_color')->distinct('variant_color')->get();
        if ($variant_color->isNotEmpty()) {
            foreach ($variant_color as $k => $v) {
                $return['color'][] = $v->variant_color;
            }
        }
        return $return;
    }

    //get jumlah produk sebuah toko
    public function ProductOfShop($id){
        $data = DB::table('t_products')->where('id_shop',$id)->where('status',1)->count();
        return $data;
    }

    //get jumlah penjualan sukses sebuah toko
    public function SuccessSales($id){
        $count = 0;
        $in_data = DB::table('t_products')->where('id_shop',$id)->pluck('id')->toArray();
        $data_all = DB::table('t_detail_transactions')->whereIn('id_product',$in_data)->count();
        $data_success = DB::table('t_detail_transactions')->whereIn('id_product',$in_data)->where('is_success',1)->count();
        if ($data_all > 0) {
            $count = (($data_success / $data_all) * 100);
        }

        return $count;
    }

    //get product by category
    public function GetProductShop($id) {
        $data = DB::table('t_products as p')->join('t_shops as s','s.id','p.id_shop')->where('id_shop',$id)->where('p.status',1)->where('main_product',1)->select('p.id','product_name','p.slug','p.viewer','is_best_seller','discount','selling_price','sold_count','stock','id_shop')->orderBy('p.created_at','desc')->limit(4)->get();
        if ($data->isNotEmpty()) {
            $code = 200;
            foreach ($data as $k => $v) {
                $data[$k]->shop_name = $this->ShopName($v->id_shop);
                $data[$k]->image = $this->GetImageProduct($v->id);
            }
        }

        return $data;
    }

    // Total feeedback
    public function TotalFeedbcak($id){
        $return = 0;
        $id_product = Product::whereNull('deleted_at')->where('id_shop',$id)->pluck('id')->toArray();
        $data = DB::table('t_user_feedbacks')->whereIn('id_product',$id_product)->count();

        return $data;
    }

    public function PositifFeedbcak($id){
        $return = 0;
        $id_product = Product::whereNull('deleted_at')->where('id_shop',$id)->pluck('id')->toArray();
        $data = DB::table('t_user_feedbacks')->whereIn('id_product',$id_product)->where('feedback_value','>',50)->count();

        return $data;
    }

    public function AvgProcessing($id){
        $trans = DB::table('t_transactions')->where('id_shop',$id)->pluck('id')->toArray();
        $data = DB::table('t_order_processing')->whereIn('id_transaction',$trans)->avg('count_of_day');
        return $data;
    }

    public function ShopName($id){
        $return = " - ";
        $data = DB::table('t_shops')->where('id',$id)->select('shop_name')->first();
        if ($data != null) {
            $return = $data->shop_name;
        }
        return $return;
    }

    public function getAge($tgl) {
        if ($tgl != null) {
            $date1 = $tgl;
            $date2 = date('Y-m-d');
            $diff = abs(strtotime($date2) - strtotime($date1));
            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
            if($years < 1){
                $data_umur = $months.' Bulan';
            } else {
                $data_umur = $years.','.$months.' Tahun';
            }
             //exit();
        } else {
            $data_umur = ' - ';
        }

        $return = $data_umur;

        return $return;
    }

    public function GetColoumnValue($id,$table,$coloumn){
        $return = "";
        $data = DB::table($table)->where('id',$id)->pluck($coloumn);
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }

    public function BaseFile($id){
        $data = DB::table('t_aslis_files')->where('id_object',$id)->whereNull('deleted_at')->select('id','file','type')->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                $data[$k]->file = env('BASE_IMG').$v->file;
            }
        }
        return $data;
    }

    public function ImageCattle($id){
        $return = 'https://souq.s3-id-jkt-1.kilatstorage.id/available_1654059656.png';
        $data = DB::table('t_aslis_files')->where('id_object',$id)->whereNull('deleted_at')->where('type',1)->pluck('file');
        if ($data->isNotEmpty()) {
            $return = env('BASE_IMG').$data[0];
        }
        return $return;
    }

    public function getKandangNow($id){
        $return = "Tidak diketahui";
        $id_kandang = DB::table('t_shed_histories')->where('id_cattle',$id)->where('status',1)->pluck('id_new_shed');
        if ($id_kandang->isNotEmpty()) {
            $data = DB::table('t_sheds')->where('id',$id_kandang[0])->pluck('shed_name');
            if ($data->isNotEmpty()) {
                $return = $data[0];
            }
        }

        return $return;

    }

    public function TFoodCattle($id,$type){
        $return = '0';

        $sum = DB::table('t_eat_histories')->where('id_cattle',$id)->whereNull('deleted_at')->sum($type);
        if ($sum > 0) {
            $return = number_format((float)$sum, 0, '.', '');
        } else {
            $return = "0";
        }

        return (string)$return;

    }

    public function TGrowthWeigth($id,$weigth){
        $berat = 0;
        $berat_awal = DB::table('t_weigth_histories')->where('id_cattle',$id)->where('is_new',1)->pluck('weigth');
        if ($berat_awal->isNotEmpty()) {
            $berat = $berat_awal[0];

        }
        $return = (float)$weigth - (float)$berat;
        if ($return > 0) {
            $return = number_format((float)$return, 2, '.', '');
        }

        return $return;
    }

}

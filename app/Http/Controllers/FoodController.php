<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use DB;

class FoodController extends Controller
{
    // Home Index
    public function index(Request $request) {
        try {
            $code = 404;
            $food = DB::table('t_foods as f')->join('t_food_categories as c','c.id','f.id_category')->where('id_user',auth('sanctum')->user()->id)->whereNull('f.deleted_at');

            if ($request->search != null) {
                $food = $food->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('food_name','ilike', "%{$search}%");
                    $query->orWhere('food_id','ilike', "%{$search}%");
                    $query->orWhere('category_name','ilike', "%{$search}%");
                });
            }

            if (!isset($request->sorting)) {
                $food = $food->orderBy('f.created_at','desc');
            }

            if ($request->sorting == 1) {
                $food = $food->orderBy('food_name','asc');
            }
            if ($request->sorting == 2) {
                $food = $food->orderBy('f.created_at','desc');
            }
            if ($request->sorting == 3) {
                $food = $food->orderBy('food_id','asc');
            }

            $food = $food->select('f.id','food_name','food_id','c.category_name','capital_price','stock','available_stock');
            $food = $food->paginate(10);
            if ($food->isNotEmpty()) {
                $code = 200;

                foreach ($food as $k => $v) {
                    $food[$k]->is_available = 0;
                    if ($v->available_stock > 0) {
                        $food[$k]->is_available = 1;
                    }
                }
            }

            $response = [
                'code' => $code,
                'data' => $food
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //history
    public function history(Request $request) {
        try {

            $code = 404;
            $food = DB::table('t_eat_histories as h')->join('t_foods as f','f.id','h.id_food')->join('t_sheds as s','s.id','h.id_shed')->join('t_cattles as c','c.id','h.id_cattle')->where('f.id_user',auth()->user()->id);

            if ($request->id_kandang != null) {
                $food = $food->where('h.id_shed',$request->id_kandang);
            }

            if ($request->id_cattle != null) {
                $food = $food->where('c.id',$request->id_cattle);
            }

            if ($request->id_food != null) {
                $food = $food->where('h.id_food',$request->id_food);
            }

            if ($request->id_history != null) {
                $food = $food->where('h.id_history',$request->id_history);
            }

            if ($request->start_date != null) {
                $food = $food->whereDate('h.date','>=',$request->start_date);
            }

            if ($request->end_date != null) {
                $food = $food->whereDate('h.date','<=',$request->end_date);
            }

            if ($request->search != null) {
                $food = $food->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('food_name','ilike', "%{$search}%");
                    $query->orWhere('food_id','ilike', "%{$search}%");
                    $query->orWhere('shed_name','ilike', "%{$search}%");
                    $query->orWhere('shed_id','ilike', "%{$search}%");
                    $query->orWhere('cattle_name','ilike', "%{$search}%");
                    $query->orWhere('cattle_id','ilike', "%{$search}%");
                });
            }

            if (!isset($request->sorting)) {
                $food = $food->orderBy('h.created_at','desc');
            }

            if ($request->sorting == 1) {
                $food = $food->orderBy('food_name','asc');
            }
            if ($request->sorting == 2) {
                $food = $food->orderBy('h.created_at','desc');
            }
            if ($request->sorting == 3) {
                $food = $food->orderBy('h.created_at','asc');
            }

            $food = $food->select('h.weigth','h.created_at','food_name','shed_name','price_food','h.date','cattle_name');
            $food = $food->paginate(10);

            if ($food->isNotEmpty()) {
                $code = 200;
                foreach ($food as $k => $v) {
                    $food[$k]->weigth = number_format((float)$v->weigth, 2, '.', '');
                    $food[$k]->price_food = number_format((float)$v->price_food, 0, '.', '');
                }
            }

            $response = [
                'code' => $code,
                'data' => $food
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@history');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //history per kandang
    public function history_shed(Request $request) {
        try {
            $code = 404;
            $food = DB::table('t_eat_per_shed as h')->join('t_foods as f','f.id','h.id_food')->join('t_sheds as s','s.id','h.id_shed')->where('h.id_user',auth()->user()->id);
            if ($request->id_kandang != null) {
                $food = $food->where('id_shed',$request->id_kandang);
            }

            if ($request->id_food != null) {
                $food = $food->where('id_food',$request->id_food);
            }

            if (!isset($request->sorting)) {
                $food = $food->orderBy('h.date','desc');
            }

            if ($request->sorting == 1) {
                $food = $food->orderBy('food_name','asc');
            }
            if ($request->sorting == 2) {
                $food = $food->orderBy('h.date','desc');
            }

            $food = $food->select('h.id','h.weigth','h.created_at','food_name','shed_name','f.price','h.date');
            $food = $food->paginate(10);
            if ($food->isNotEmpty()) {
                $code = 200;
                foreach ($food as $k => $v) {
                    $food[$k]->weigth = number_format((float)$v->weigth, 2, '.', '');
                    $food[$k]->price = $this->TotalPricefood($v->id);
                }
            }

            $response = [
                'code' => $code,
                'data' => $food
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@history_shed');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function TotalPricefood($id){
        $sum = DB::table('t_eat_histories')->where('id_history',$id)->sum('price_food');
        if ($sum > 0) {
            $return = number_format((float)$sum, 0, '.', '');
        } else {
            $return = "0";
        }
        return $return;
    }

    //detail//
    public function detail(Request $request){
        try {
            $data = DB::table('t_foods')->where('id',$request->id)->first();
            $data->category_name = $this->CategoryFood($data->id_category);
            // $data->used_stock = $this->CountStock($data->stock,$data->available_stock,1);
            // $data->prosentase = $this->CountStock($data->stock,$data->available_stock,2);
            $data->is_available = 0;
            if ($data->available_stock > 0) {
                $data->is_available = 1;
            }

            $total = (($data->stock - $data->available_stock) / $data->stock );
            $data->total = number_format((float)$total, 2, '.', '');

            $total_stok = $data->stock - $data->available_stock;
            $data->total_stock = number_format((float)$total_stok, 2, '.', '');

            $response = [
                'data' =>$data,
                'message' => "Get Data"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@detail');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //pilih pakan
    public function choose(Request $request){
        $data = DB::table('t_foods')->where('id',$request->id)->first();
        $response = [
            'stock' =>$data->available_stock,
        ];
        return response($response,200);
    }

    // public function CountStock($stock,$ava,$type){
    //     $return = 0;
    //     if ($type == 1) {
    //         $data = $stock - $ava;
    //         $return = $data;
    //     } else {

    //     }
    // }

    public function CategoryFood($id){
        $return = "";
        $data = DB::table('t_food_categories')->where('id',$id)->pluck('category_name');
        if ($data->isNotEmpty()) {
           $return = $data[0];
        }
        return $return;

    }

    // tambah pakan
    public function add(Request $request) {
        try {
            $input = $request->except('_token');


            $input['available_stock'] = $request->available_stock;
            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_foods')->insertGetId($input);

            $response = [
                'data' => $input,
                'message' => "Berhasil menambah pakan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // update pakan
    public function update(Request $request) {
        try {
            $input = $request->except('_token');
            $input['available_stock'] = $request->stock;
            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_foods')->where('id',$request->id)->update($input);

            $response = [
                'data' => $input,
                'message' => "Berhasil menambah pakan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // delete pakan
    public function delete(Request $request) {
        try {

            $insert = DB::table('t_foods')->where('id',$request->id)->update(['status'=>0,'deleted_at'=>date('Y-m-d')]);

            $response = [
                'message' => "Berhasil menghapus pakan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@delete');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // set status pakan
    public function set_status(Request $request) {
        try {

            $insert = DB::table('t_foods')->where('id',$request->id)->update(['status'=>$request->status,'updated_at'=>date('Y-m-d H:i:s')]);

            $response = [
                'message' => "Berhasil menghapus pakan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@set_status');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //detail_history
    public function detail_history(Request $request){
        try {
            $data = DB::table('t_eat_per_shed')->where('id',$request->id)->first();
            $data->food_name = parent::GetColoumnValue($data->id_food,'t_foods','food_name');
            $data->shed_name = parent::GetColoumnValue($data->id_shed,'t_sheds','shed_name');

            $response = [
                'data' =>$data,
                'message' => "Get Data"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@detail');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }

    }
    //update history pakan
    public function update_history(Request $request) {
        try {
            $in_shed = DB::table('t_shed_histories as s')->join('t_cattles as c','c.id','s.id_cattle')->where('c.status',1)->where('s.status',1)->where('id_new_shed',$request->id_shed)->pluck('id_cattle');
            if ($in_shed->isEmpty()) {
                return response(['message'=>'Maaf kandang tersebut sudah kosong'],500);
            } else {
                //cek ketersediaan pakan
                $history = DB::table('t_eat_per_shed')->where('id',$request->id)->first();
                $cek = DB::table('t_foods')->where('id',$request->id_food)->select('capital_price','available_stock','stock')->get();
                $stock_sem = $cek[0]->available_stock + $history->weigth;
                if ($request->weigth <= $stock_sem) {
                    //insert history
                    $inputHistory = ['id_shed'=>$request->id_shed,
                                  'id_food'=>$request->id_food,
                                  'id_user'=>auth('sanctum')->user()->id,
                                  'date'=>$request->date,
                                  'weigth'=>$request->weigth,
                                  'created_at'=>date('Y-m-d H:i:s'),
                                  'updated_at'=>date('Y-m-d H:i:s')
                                 ];
                    $id_history = DB::table('t_eat_per_shed')->where('id',$request->id)->update($inputHistory);

                    $cattle = DB::table('t_cattles')->whereIn('id',$in_shed->toArray())->get();
                    DB::table('t_eat_histories')->where('id_history',$request->id)->delete();
                    foreach ($cattle as $k => $v) {
                        $price_in  = $cek[0]->capital_price / $cek[0]->stock;
                        $weigth = $request->weigth / count($cattle);
                        $price = $price_in * $weigth;
                        //histori makan
                        $input = ['id_shed'=>$request->id_shed,
                                  'id_food'=>$request->id_food,
                                  'id_cattle'=>$v->id,
                                  'id_history'=>$request->id,
                                  'price_food'=>$price,
                                  'date'=>$request->date,
                                  'weigth'=>$weigth,
                                  'created_at'=>date('Y-m-d H:i:s'),
                                  'updated_at'=>date('Y-m-d H:i:s')
                                 ];
                        $insert = DB::table('t_eat_histories')->insertGetId($input);
                        $response = [
                            'message' => "Berhasil memberi makan ternak"
                        ];
                    }
                    // kurangi stock pakan
                    $stock_now = $stock_sem - $request->weigth;
                    $update_stock = DB::table('t_foods')->where('id',$request->id_food)->update(['available_stock'=>$stock_now]);
                } else {
                    return response(['message'=>'Maaf stock hanya tersedia sebesar '.$cek[0]->available_stock],500);
                }

            }

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'CattleController@give_food');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // delete history pakan
    public function delete_history(Request $request) {
        try {
            //kembalikan stock
            $history = DB::table('t_eat_per_shed')->where('id',$request->id)->first();
            $cek = DB::table('t_foods')->where('id',$history->id_food)->pluck('available_stock');
            $stock_now = $cek[0] + $history->weigth;
            $update_stock = DB::table('t_foods')->where('id',$history->id_food)->update(['available_stock'=>$stock_now]);

            $insert = DB::table('t_eat_per_shed')->where('id',$request->id)->update(['deleted_at'=>date('Y-m-d')]);
            $delete = DB::table('t_eat_histories')->where('id_history',$request->id)->delete();

            $response = [
                'message' => "Berhasil menghapus pakan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'FoodController@delete');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\Country;
use App\Models\Province;
use App\Models\City;
use App\Models\District;
use App\Models\Village;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Auth;
use App\Classes\S3;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class GetDataController extends Controller
{
    //home
    public function home(Request $request) {
        try {
            $now = date('Y-m-d',strtotime($request->year.'-'.$request->month.'-'.'01'));
            $now = strtotime($now);
            $last_month = date("m", strtotime("-1 month", $now));
            $last_year = date("Y", strtotime("-1 month", $now));
            $catle_now = $this->CattleNumber($request->year,$request->month);
            $catle_last = $this->CattleNumber($last_year,$last_month);
            $progress_now = $this->ProgressNumber($request->year,$request->month);
            $progress_last = $this->ProgressNumber($last_year,$last_month);
            $weigth_now = $this->WeigthNumber($request->year,$request->month);
            $weigth_last = $this->WeigthNumber($last_year,$last_month);

            $data['cattle_number'] = $catle_now;
            $data['grouwth'] = $progress_now;
            $data['weigth']   = $weigth_now;

            $data['cattle_number_progress'] = 0;
            $data['grouwth_progress'] =  0;
            $data['weigth_progress'] = 0;

            if ($catle_now > 0) {
                $data['cattle_number_progress'] = 100;
            }

            if ($progress_now > 0) {
                $data['grouwth_progress'] =  100;
            }

            if ($weigth_now > 0) {
                $data['weigth_progress'] = 100;
            }

            if($catle_last > 0){
                 $data['cattle_number_progress'] = (($catle_now - $catle_last) / $catle_last) * 100;
            }
            if($progress_last > 0){
                $data['grouwth_progress'] =  (($progress_now - $progress_last) / $progress_last) * 100;
            }
            if($weigth_last > 0){
                $data['weigth_progress'] = (($weigth_now - $weigth_last) / $weigth_last) * 100;
            }

            $response = [
                'data' => $data,
                'messages' => "Success"
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'GetDataController@home');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function CattleNumber($year,$month){
        $data = DB::table('t_cattles')->where('id_user',auth()->user()->id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->count();
        return $data;
    }

    public function WeigthNumber($year,$month){
        $data = DB::table('t_cattles')->where('id_user',auth()->user()->id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->sum('weigth');
        return (float)$data;
    }

    public function ShedNumber($year,$month){
        $data = DB::table('t_sheds')->where('id_user',auth()->user()->id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->count();
        return $data;
    }

    public function ProgressNumber($year,$month){
        $data = DB::table('t_cattles')->where('id_user',auth()->user()->id)->whereNull('deleted_at')->whereYear('created_at',$year)->whereMonth('created_at',$month)->where('type',1)->count();
        return $data;
    }

	//get data country
    public function country(Request $request) {

    	$data = Country::where('status',1)->select('id','name')->get();
    	$response = [
    		'data' => $data,
    		'messages' => "Success"
    	];

    	return response($response,200);
    }

    //get data province
    public function province(Request $request) {
    	//$id = parent::cleanHazard($id);

    	$data = Province::where('status',1)->select('id','name')->orderBy('name','asc')->get();
    	$response = [
    		'data' => $data,
    		'messages' => "Success"
    	];

    	return response($response,200);
    }
    //get data city
    public function city(Request $request,$id) {
        $id = parent::cleanHazard($id);

        $data = City::where('status',1)->where('province_id',$id)->select('id','name')->orderBy('name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }
    //get data kecamatan
    public function district(Request $request,$id) {
        $id = parent::cleanHazard($id);

        $data = District::where('status',1)->where('regency_id',$id)->select('id','name')->orderBy('name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }
    //get data desa
    public function village(Request $request,$id) {
        $id = parent::cleanHazard($id);

        $data = Village::where('status',1)->where('district_id',$id)->select('id','name')->orderBy('name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }
    //get data category cattle
    public function category_cattle() {

        $data = DB::table('t_cattle_categories')->where('status',1)->select('id','category_name as name')->orderBy('category_name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }

    //category_food
    public function category_food() {

        $data = DB::table('t_food_categories')->where('status',1)->select('id','category_name as name')->orderBy('category_name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }

    //get data induk jantan
    public function male_parent() {

        $data = DB::table('t_cattles')->where('id_user',auth()->user()->id)->where('status',1)->where('gender',1)->select('id','cattle_name','cattle_id')->orderBy('cattle_name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }

    //get data induk jantan
    public function female_parent() {

        $data = DB::table('t_cattles')->where('id_user',auth()->user()->id)->where('status',1)->where('gender',2)->select('id','cattle_name','cattle_id')->orderBy('cattle_name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }


    public function ProductNumberCategory($id){
        $return = DB::table('relation_product_category')->where('id_category',$id)->distinct('id_product')->count();
        return $return;
    }


    //cari kota by nama
    public function search_city(Request $request){
        $search = parent::cleanHazard($request->search);

        $data = City::where('status',1)->where('name','ilike', "%{$search}%")->orderBy('name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);
    }

    //shed
    public function shed(){
        $data = DB::table('t_sheds')->where('status',1)->where('id_user',auth('sanctum')->user()->id)->select('id','shed_id','shed_name')->orderBy('shed_name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);

    }

    //cattle
    public function cattle(){
        $data = DB::table('t_cattles')->where('status',1)->where('id_user',auth('sanctum')->user()->id)->select('id','cattle_id','cattle_name')->orderBy('cattle_name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);

    }

    //food
    public function food(){
        $data = DB::table('t_foods')->where('status',1)->where('id_user',auth('sanctum')->user()->id)->select('id','food_id','food_name')->orderBy('food_name','asc')->get();
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];

        return response($response,200);

    }

    // update position lat long
    public function update_position(Request $request){
        $input = $request->except('_token');
        try {
            $id_user = null;
            if (auth('sanctum')->check() == true) {
                $input['id_user'] = auth('sanctum')->user()->id;
                $update = DB::table('t_users')->where('id',auth('sanctum')->user()->id)->update(['latitude'=>$request->latitude,'longitude'=>$request->longitude,'last_active'=>date('Y-m-d H:i:s')]);
            }
            $input['created_at'] = date('Y-m-d');
            $input = DB::table('t_user_positions')->insert($input);

            $response = [
                'messages' => "Success"
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'GetDataController@update_position');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    //events
    public function master_events(){
        try {
            $data = DB::table('t_events')->where('status',1)->select('id','event_name')->orderBy('event_name','asc')->get();
            $response = [
                'data' => $data
            ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'GetDataController@master_events');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //events
    public function master_problems(){
        try {
            $data = DB::table('t_master_problems')->where('status',1)->select('id','problem')->orderBy('problem','asc')->get();
            $response = [
                'data' => $data
            ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'GetDataController@master_problems');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    //laporan_pakan
    public function laporan_pakan(){
        $token = 'RHS#!098_HJ$cba01';
        $id = base64_encode(auth('sanctum')->user()->id);
        $link = 'https://cms-aslis.angkitagro.com/laporan_pakan/'.$id.'/'.$token;
        $response = [
            'data' => $link
        ];
        return response($response,200);
    }

    //laporan_pakan
    public function laporan_ternak(){
        $token = 'RHS#!098_HJ$cba01';
        $id = base64_encode(auth('sanctum')->user()->id);
        $link = 'https://cms-aslis.angkitagro.com/laporan_ternak/'.$id.'/'.$token;
        $response = [
            'data' => $link
        ];
        return response($response,200);
    }

    public function weigth_noted_from_machine_old(Request $request){

        $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
        //skip first line
        fgetcsv($csvFile);
        //parse data from csv file line by line
        //$data = fgetcsv($csvFile);
        while(($line = fgetcsv($csvFile)) !== FALSE){
            // $cek = DB::table('address')->where('address',$line[1])->get();
            // if ($cek->isEmpty()) {

            //dd($line[1]);
           // }
            $response = [
            'data' => $line[1],
            'message' => 'success'
            ];
            return response($response,200);
        }


        fclose($csvFile);
    }

    public function weigth_noted_from_machine(Request $request){

        $csvFile = fopen($_FILES['file']['tmp_name'], 'r');
        //skip first line
        fgetcsv($csvFile);
        //parse data from csv file line by line
        //$data = fgetcsv($csvFile);
        while(($line = fgetcsv($csvFile)) !== FALSE){
            $id_user = DB::table('t_devices')->where('device_id',$line[2])->pluck('id_user');
            if ($id_user->isNotEmpty()) {
                $cattles = DB::table('t_cattles')->where('cattle_id',$line[4])->first();
                $id_kandang = DB::table('t_shed_histories')->where('status',1)->where('id_cattle',$cattles->id)->pluck('id_new_shed');
                if ($id_kandang->isNotEmpty()) {
                    //save DB
                    $cek = DB::table('t_weigth_per_shed')->where('id_shed',$id_kandang[0])->where('id_user',$id_user[0])->whereDate('date',date('Y-m-d', strtotime($line[1])))->get();
                        if ($cek->isEmpty()) {
                            $inputHistory = ['id_shed'=>$id_kandang[0],
                                  'id_user'=>$id_user[0],
                                  'date'=>$line[1],
                                  'created_at'=>$line[1],
                                  'updated_at'=>$line[1]
                                 ];
                            $id_history = DB::table('t_weigth_per_shed')->insertGetId($inputHistory);
                        } else {
                            $id_history = $cek[0]->id;
                        }

                        $image = null;
                        if ($line[3] != '') {
                            $image = $this->ThisUploadFileS3($line[3]);  // your base64 encoded
                        }

                        $data_catatan = ['id_cattle'=>$cattles->id,
                            'id_shed'=>$id_kandang[0],
                            'id_history'=>$id_history,
                            'date'=>$line[1],
                            'weigth'=>(float)$line[5],
                            'photo'=>$image,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
                        ];

                        $insert = DB::table('t_weigth_histories')->insertGetId($data_catatan);
                        DB::table('t_cattles')->where('id',$cattles->id)->update(['weigth'=>(float)$line[5]]);
                } else {
                    return response(['message'=>'ID Hewan Ternak Tidak ditemukan'],500);
                }
            } else {
                return response(['message'=>'ID Device Tidak ditemukan'],500);
            }

        }

        $response = [
        'data' => date('Y-m-d H:i:s'),
        'message' => 'success'
        ];
        return response($response,200);

        fclose($csvFile);
    }

    public function ThisUploadFileS3($photo){
        $acces_key = env('AWS_ACCESS_KEY_ID');
        //dd($acces_key);
        $secret_key = env('AWS_SECRET_ACCESS_KEY');
        $s3 = new S3($acces_key,$secret_key);
        $bucket = env('AWS_BUCKET');

        $image = $photo;  // your base64 encoded
        $image = substr($image, 2);
        $image = substr($image, 0, -1);
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'kambing_'.date('YmdHis').'.'.'png';

        // $file_path = public_path('images/'.$imageName);
        \File::put(public_path(). '/' . $imageName, base64_decode($image));

        return $imageName;
    }

}


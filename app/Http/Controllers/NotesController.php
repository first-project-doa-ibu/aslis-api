<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use DB;

class NotesController extends Controller
{
    // Home Index
    public function index(Request $request) {
        try {
            $code = 404;
            $notes = DB::table('t_notes')->whereNull('deleted_at')->where('id_user',auth('sanctum')->user()->id);

            if ($request->search != null) {
                $notes = $notes->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('title','ilike', "%{$search}%");
                    $query->orWhere('description','ilike', "%{$search}%");
                });
            }
            
            if ($request->sorting == 1) {
                $notes = $notes->orderBy('title','asc');
            }
            if ($request->sorting == 2) {
                $notes = $notes->orderBy('date','desc');
            }

            $notes = $notes->select('id','title','date');
            $notes = $notes->paginate(10);
            if ($notes->isNotEmpty()) {
                $code = 200;
            }
            
            $response = [
                'code' => $code,
                'data' => $notes
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotesController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //detail//
    public function detail(Request $request){
        try {
            $data = DB::table('t_notes')->where('id',$request->id)->first();
            if($data->photo != null){
                $data->photo = env('BASE_IMG').$data->photo;
            }
                $data->event_name = parent::GetColoumnValue($data->id_event,'t_events','event_name');
                $data->problem_name = parent::GetColoumnValue($data->id_problem,'t_master_problems','problem');
                $data->data_problem = $this->ProblemNotes($data->id);
            //}
            $response = [
                'data' =>$data,
                'message' => "Get Data"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotesController@detail');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function ProblemNotes($id){
        $return = [];
        $data = DB::table('t_note_problems')->where('id_note',$id)->select('id_object as id','type')->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                if ($v->type == 1) {
                    $data[$k]->nama = parent::GetColoumnValue($v->id,'t_sheds','shed_name');
                    $data[$k]->code = parent::GetColoumnValue($v->id,'t_sheds','shed_id');

                } else if ($v->type == 2) {
                    $data[$k]->nama = parent::GetColoumnValue($v->id,'t_cattles','cattle_name');
                    $data[$k]->code = parent::GetColoumnValue($v->id,'t_sheds','shed_id');
                } else {
                    $data[$k]->nama = parent::GetColoumnValue($v->id,'t_foods','food_name');
                    $data[$k]->code = parent::GetColoumnValue($v->id,'t_sheds','shed_id');
                }
                $data[$k]->name = $data[$k]->nama.' -('.$data[$k]->code.')';
            }
            $return = $data;
        }
        return $return;
    }

    // tambah catatan
    public function add(Request $request) {
        try {
            $input = $request->except('_token','id_kandang','id_hewan','id_makanan');
            $photo = null;
            if($request->hasfile('photo'))
            {
                $photo = parent::uploadFileS3($request->file('photo'));
            }
            $input['photo'] = $photo;
            
            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_notes')->insertGetId($input);
            if (isset($request->id_kandang)) {
                $kandang = $request->id_kandang;
                if ($kandang[0] != null || $kandang[0] != "") {
                    for ($i=0; $i < count($kandang); $i++) { 
                        $data_insert = ["id_note"=>$insert,"id_object"=>$kandang[$i],"type"=>1,"created_at"=>date('Y-m-d H:i:s')];
                        DB::table('t_note_problems')->insert($data_insert);
                    }
                }
            }

            if (isset($request->id_hewan)) {
                $hewan = $request->id_hewan;
                if ($hewan[0] != null || $hewan[0] != "") {
                    for ($i=0; $i < count($hewan); $i++) { 
                        $data_insert = ["id_note"=>$insert,"id_object"=>$hewan[$i],"type"=>2,"created_at"=>date('Y-m-d H:i:s')];
                        DB::table('t_note_problems')->insert($data_insert);
                    }
                }
            }

            if (isset($request->id_makanan)) {
                $makanan = $request->id_makanan;
                if ($makanan[0] != null || $makanan[0] != "") {
                    for ($i=0; $i < count($makanan); $i++) { 
                        $data_insert = ["id_note"=>$insert,"id_object"=>$makanan[$i],"type"=>3,"created_at"=>date('Y-m-d H:i:s')];
                        DB::table('t_note_problems')->insert($data_insert);
                    }
                }
            }

            $response = [
                'data' => $input,
                'message' => "Berhasil menambah catatan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotesController@add');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // update catatan
    public function update(Request $request) {
        try {
            $input = $request->except('_token','id_kandang','id_hewan','id_makanan');
            $existing = DB::table('t_notes')->where('id',$request->id)->first();
            $photo = $existing->photo;
            if($request->hasfile('photo'))
            {
                $photo = parent::uploadFileS3($request->file('photo'));
            }
            $input['photo'] = $photo;
            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_notes')->where('id',$request->id)->update($input);
            
            DB::table('t_note_problems')->where('id_note',$request->id)->delete();
            if (isset($request->id_kandang)) {
                $kandang = $request->id_kandang;
                if ($kandang[0] != null || $kandang[0] != "") {
                    for ($i=0; $i < count($kandang); $i++) { 
                        $data_insert = ["id_note"=>$request->id,"id_object"=>$kandang[$i],"type"=>1,"created_at"=>date('Y-m-d H:i:s')];
                        DB::table('t_note_problems')->insert($data_insert);
                    }
                }
            }

            if (isset($request->id_hewan)) {
                $hewan = $request->id_hewan;
                if ($hewan[0] != null || $hewan[0] != "") {
                    for ($i=0; $i < count($hewan); $i++) { 
                        $data_insert = ["id_note"=>$request->id,"id_object"=>$hewan[$i],"type"=>2,"created_at"=>date('Y-m-d H:i:s')];
                        DB::table('t_note_problems')->insert($data_insert);
                    }
                }
            }

            if (isset($request->id_makanan)) {
                $makanan = $request->id_makanan;
                if ($makanan[0] != null || $makanan[0] != "") {
                    for ($i=0; $i < count($makanan); $i++) { 
                        $data_insert = ["id_note"=>$request->id,"id_object"=>$makanan[$i],"type"=>3,"created_at"=>date('Y-m-d H:i:s')];
                        DB::table('t_note_problems')->insert($data_insert);
                    }
                }
            }

            $response = [
                'data' => $input,
                'message' => "Berhasil menambah catatan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotesController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // delete catatan
    public function delete(Request $request) {
        try {
            
            $insert = DB::table('t_notes')->where('id',$request->id)->update(['status'=>0,'deleted_at'=>date('Y-m-d')]);

            $response = [
                'message' => "Berhasil menghapus catatan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotesController@delete');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // set status catatan
    public function set_status(Request $request) {
        try {
            
            $insert = DB::table('t_notes')->where('id',$request->id)->update(['status'=>$request->status,'updated_at'=>date('Y-m-d H:i:s')]);

            $response = [
                'message' => "Berhasil menghapus catatan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotesController@set_status');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function from_machine(){
        $curl = curl_init();
        $headers = array();
        $headers = array ('Authentication-Key: foobar','Content-Type: application/json','verif_token: EX_001klmnay!YZz001');
        // dd($headers);
        $end_point = 'https://api-aslis.angkitagro.com/api/weight_noted_from_machine';
        $data['id'] = '';
        $data['datetime'] = '';
        $data['device_id'] = '';
        $data['image'] = '';
        $data['rfid'] = '';
        $data['scale'] = '';

        $payload = json_encode($data); 
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_USERPWD, "");
        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        $responseObject = json_encode($response, true);
    }
}

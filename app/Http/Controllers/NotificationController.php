<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use DB;
use GeniusTS\HijriDate\Date;

class NotificationController extends Controller
{
    // Notifikasi
    public function index(Request $request) {
        try {
            $id_user = auth()->user()->id;
            $in_view = DB::table('t_user_read_notive')->where('id_user',auth()->user()->id)->pluck('id_notification')->toArray();
            $data['data'] = DB::table('t_notifications')->whereNotIn('id',$in_view)->where(function ($query) use ($request,$id_user) {
                                    $query->where('id_user',$id_user);
                                    $query->orWhere('is_umum',1);
                                })->select('id','id_detail','title','tipe','text')->paginate(10);
            foreach ($data['data'] as $k => $v) {
                $data['data'][$k]->text = strip_tags($v->text);
            }
            return response($data,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotificationController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // Remove Notifikasi
    public function delete(Request $request) {
        try {
            $datas = [
                "id_user" => auth()->user()->id,
                "id_notification" => $request->id_notification,
                "read_at"   =>  date('Y-m-d H:i:s'),
            ];
            $simpan = DB::table('t_user_read_notive')->insertGetId($datas); 
            $data['message'] = "Berhasil Menghapus notifikasi";
            return response($data,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'NotificationController@remove');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

}

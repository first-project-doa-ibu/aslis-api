<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\WareHouse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

class ProductController extends Controller
{
    //get data product
    public function index(Request $request) {

    	$product = Product::where('status',1)->where('main_product',1)->select('id','product_name','slug','viewer','is_best_seller','discount','selling_price','sold_count','stock','last_price')->orderBy('created_at','desc')->get();
    	$response = [
    		'data' => $product,
    		'messages' => "Success"
    	];

    	return response($response,200);
    }

    //get_product
    public function get_product(Request $request) {

        try {
            $code = 404;
            $product = DB::table('t_products as p')->join('t_shops as s','s.id','p.id_shop')->where('p.status',1)->where('p.stock','>',0)->where('main_product',1);
            if (isset($request->id_category)) {
                if (count($request->id_category) > 0) {
                    $in_array = DB::table('relation_product_category')->whereIn('id_category',$request->id_category)->pluck('id_product')->toArray();
                    $product = $product->whereIn('p.id',$in_array);
                }
                
            }

            if ($request->id_shop != null) {
                $product = $product->where('id_shop',$request->id_shop);
            }

            if ($request->is_gerai != null) {
                $product = $product->where('is_gerai_official',$in_city);
            }

            if (isset($request->id_city)) {
                if (count($request->id_city) > 0) {
                    $in_array_city = DB::table('relation_product_warehouses as r')->join('t_warehouses as w','w.id','r.warehouse_id')->whereIn('w.id_city',$request->id_city)->pluck('product_id')->toArray();
                    $product = $product->whereIn('p.id',$in_array_city);
                }
            }

            if ($request->rating != null) {
                if ($request->rating == 5) {
                    $product = $product->where('p.rating',5);
                } else if ($request->rating == 4) {
                    $product = $product->where('p.rating','>=',4)->where('p.rating','<',5);
                } else if ($request->rating == 3) {
                    $product = $product->where('p.rating','>=',3)->where('p.rating','<',4);
                } else if ($request->rating == 2) {
                    $product = $product->where('p.rating','>=',2)->where('p.rating','<',3);
                } else if ($request->rating == 1) {
                    $product = $product->where('p.rating','>=',2)->where('p.rating','<',1);
                } else {
                    $product = $product->where('p.rating',0);
                }
                
            }

            if (isset($request->id_etalase)) {
                if (count($request->id_etalase) > 0) {
                    $in_promotion = DB::table('relation_product_etalase')->where('id_etalase',$request->id_etalase)->pluck('id_product')->toArray();
                    $product = $product->whereIn('p.id',$in_promotion);
                }
            }
            if (isset($request->id_delivery)) {

                $in_delivery = DB::table('t_master_deliveries')->whereIn('group',$request->id_delivery)->pluck('id')->toArray();
                $in_delivery_product = DB::table('relation_delivery_products')->whereIn('id_delivery',$in_delivery)->pluck('id_product')->toArray();
                $product = $product->whereIn('p.id',$in_delivery_product);
            }

            if ($request->id_promotion != null) {
                $in_promotion = DB::table('relation_product_sales')->where('id_master_sales',$request->id_promotion)->pluck('id_product')->toArray();
                $product = $product->whereIn('p.id',$in_promotion);
            }

            if ($request->search != null) {
                $product = $product->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('product_name','ilike', "%{$search}%");
                    $query->orWhere('p.slug','ilike', "%{$search}%");
                });
            }

            // $id_produk_gudang = $product->pluck('p.id')->toArray();
            // $in_array_gudang = DB::table('relation_product_warehouses')->whereIn('product_id',$id_produk_gudang)->pluck('warehouse_id');
            // $gudang = DB::table('t_warehouses as r')->join('cities as c','c.id','r.id_city')->whereIn('r.id',$in_array_gudang)->where('c.status',1)->select('c.id','c.name')->get();
            
            if ($request->sorting == 1) {
                $product = $product->orderBy('p.viewer','desc');
            }
            if ($request->sorting == 2) {
                $product = $product->orderBy('p.created_at','desc');
            }
            if ($request->sorting == 3) {
                $product = $product->orderBy('sold_count','desc');
            }

            if ($request->sorting_price == 1) {
                $product = $product->orderBy('p.last_price','desc');
            }

            if ($request->sorting_price == 2) {
                $product = $product->orderBy('p.last_price','asc');
            }

            $product = $product->select('p.id','product_name','p.slug','p.viewer','is_best_seller','discount','selling_price','sold_count','stock','id_shop','p.rating','shop_name','pre_order','last_price');

            $product = $product->paginate(6);
            if ($product->isNotEmpty()) {
                $code = 200;
                foreach ($product as $k => $v) {
                    $product[$k]->image = parent::GetImageProduct($v->id);
                }
            }
            
            $response = [
                'code' => $code,
                'data' => $product
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@get_product');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get_diskon
    public function get_diskon(Request $request) {

        try {
            $code = 404;
            $data = DB::table('t_products as p')->join('t_shops as s','s.id','p.id_shop')->where('discount','>',0)->where('p.status',1)->where('p.stock','>',0)->where('main_product',1)->select('p.id','product_name','p.slug','p.viewer','is_best_seller','discount','selling_price','sold_count','stock','id_shop','p.rating','shop_name','pre_order')->paginate(6);
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    $data[$k]->image = parent::GetImageProduct($v->id);
                }
            }
            
            $response = [
                'code' => $code,
                'data' => $data
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@get_diskon');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get_product terbaru
    public function get_product_newly(Request $request) {

        try {
            $code = 404;
            $data = Product::where('status',1)->where('main_product',1)->where('p.stock','>',0)->select('id','product_name','slug','viewer','is_best_seller','discount','selling_price','sold_count','stock','id_shop','pre_order')->orderBy('created_at','desc')->paginate(6);
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    $data[$k]->shop_name = parent::ShopName($v->id_shop);
                    $data[$k]->image = parent::GetImageProduct($v->id);
                }
            }
            
            $response = [
                'code' => $code,
                'data' => $data
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@get_product');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get product by category
    public function product_by_category(Request $request) {

        try {
            $code = 404;
            $id_user = null;
            if (auth('sanctum')->check() == true) {
                $id_user = auth('sanctum')->user()->id;
            }
            $data = DB::table('t_products as p')->join('relation_product_category as r','r.id_product','p.id')->where('id_category',$request->id)->where('status',1)->where('main_product',1)->where('p.stock','>',0)->select('p.id','product_name','slug','viewer','is_best_seller','discount','selling_price','sold_count','stock','id_shop','stock','pre_order')->orderBy('created_at','desc')->paginate(6);
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    $data[$k]->shop_name = parent::ShopName($v->id_shop);
                    $data[$k]->image = parent::GetImageProduct($v->id);
                }
            }
            //insert data untuk dikelola
            $insertSystem = parent::insertSystem(3,$request->id,$id_user,$request->ip_device,$request->device_brand);
            $response = [
                'code' => $code,
                'data' => $data
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@get_product');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get product by get_product_shop
    public function get_product_shop(Request $request) {

        try {
            $code = 404;
            
            $data = parent::GetProductShop($request->id);
            if ($data->isNotEmpty()) {
                $code = 200;
                
            }
            //insert data untuk dikelola
            $response = [
                'code' => $code,
                'data' => $data
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@get_product');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get product by get_product_shop
    public function get_product_shop_paginnation(Request $request) {

        try {
            $code = 404;
            
            $data = DB::table('t_products as p')->join('t_shops as s','s.id','p.id_shop')->where('id_shop',$request->id)->where('p.status',1)->where('p.stock','>',0)->where('main_product',1)->select('p.id','product_name','p.slug','p.viewer','is_best_seller','discount','selling_price','sold_count','pre_order')->orderBy('p.created_at','desc')->paginate(6);
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    $data[$k]->category = $this->GetCaegoryProduct($v->id);
                    $data[$k]->image = $this->GetImageProduct($v->id);
                }
            }
            //insert data untuk dikelola
            $response = [
                'code' => $code,
                'data' => $data
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@get_product');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get product by promotion
    
    //tambah gudang
    public function add_warehouse(Request $request) {

    	try {
	        $input = $request->except('_token');
	        $input['id_user'] = auth()->user()->id;
	        $create_id = DB::table('t_warehouses')->InsertGetId($input);
	    	$response = [
	    		'id' => $create_id,
	    		'messages' => "Success menambah gudang baru"
	    	];
	    	parent::LogSystem(' Berhasil menambah gudang baru -'.$request->warehouses_name,auth()->user()->id,'Menambah Gudang',2,$create_id);

	    	return response($response,200);
    	} catch (\Exception $err) {
    		$error = $err->getMessage();
    		//Insert Log Error
    		parent::LogErrorCreate($error,'ProductController@add_warehouse');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //edit gudang
    public function edit_warehouse(Request $request) {

        try {
            $input = $request->except('_token');
            $update = DB::table('t_warehouses')->where('id',$request->id)->update($input);
            $response = [
                'id' => $request->id,
                'messages' => "Success mengedit gudang baru"
            ];
            parent::LogSystem(' Berhasil mengedit gudang -'.$request->warehouses_name,auth()->user()->id,'Menambah Gudang',2,$request->id);

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@edit_warehouse');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get data gudang
    public function get_warehouse(Request $request) {

    	try {
	        $data = WareHouse::whereNull('deleted_at')->where('id_user',auth()->user()->id)->select('id','warehouses_name','address','pos_code','latitude','longitude','email','phone')->get();
	    	$response = [
	    		'data' => $data
	    	];

	    	return response($response,200);
    	} catch (\Exception $err) {
    		$error = $err->getMessage();
    		//Insert Log Error
    		parent::LogErrorCreate($error,'HomeController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get data gudang by ID
    public function get_warehouse_by_id(Request $request) {

        try {
            $data = WareHouse::where('id',$request->id)->select('id','warehouses_name','address','pos_code','latitude','longitude','email','phone','id_province','id_city','id_district','id_village')->first();
            $data->province_name = parent::GetAddressName('provinces',$data->id_province);
            $data->city_name     = parent::GetAddressName('cities',$data->id_city);
            $data->district_name = parent::GetAddressName('districts',$data->id_district);
            $data->village_name  = parent::GetAddressName('villages',$data->id_village);
            $response = [
                'data' => $data
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'HomeController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get delete gudang by ID
    public function delete_warehouse(Request $request) {

        try {
            $delete = WareHouse::where('id',$request->id)->update(['deleted_at'=>date('Y-m-d'),'status'=>0]);
            $response = [
                'message' => "Berhasil Menghapus Gudang"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@delete_warehouse');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    
    //tambah kategori produk
    public function add_category(Request $request) {

        try {
            $fields = $request->validate([
                'category_name' => 'required|string'
            ]);
            $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$request->category_name);
            $slug  = strtolower($slug);
            $icon = null;
            if($request->hasfile('icon'))
            {
                $icon = parent::uploadFileS3($request->file('icon'));
            }
            $insert = DB::table('t_category_products')->insertGetId(['category_product_name'=>$request->category_name,'description'=>$request->description,'slug'=>$slug,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'category_product_icon'=>$icon]);
            $response = [
                'id' => $insert,
                'name' => $request->category_name
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@add_category');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

   //upload image product
    public function upload_image(Request $request) {

        try {
            $fields = $request->validate([
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:5048'
            ]);
            if($request->hasfile('image'))
            {
                $image = parent::uploadFileS3($request->file('image'));
            }
            $response = [
                'name' => $image,
                'url' => env('BASE_IMG').$image
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@add_category');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // Tambah produk baru
    public function add(Request $request) {

        try {
            $data = json_decode(request()->getContent(), true);
            
            //nama wajib diisi
            if ($data['product_name'] == null || $data['product_name'] == '') {
                return response (['message' => 'Nama Produk Wajib Diisi'], 500);
            }
            //id category wajib minimal 1
            if (count($data['id_category']) == 0) {
                return response (['message' => 'Kategori Wajib Diisi'], 500);
            }
            //nama wajib diisi
            if ($data['selling_price'] == null || $data['selling_price'] == '') {
                return response (['message' => 'Harga Wajib Diisi'], 500);
            }
            //nama wajib diisi
            if ($data['stock'] == null || $data['stock'] == '') {
                return response (['message' => 'Stock Produk Wajib Diisi'], 500);
            }
            //Gudangy wajib minimal 1
            if (count($data['id_warehouse']) == 0) {
                return response (['message' => 'Gudang Wajib Diisi'], 500);
            }
            //Delivery wajib minimal 1
            if (count($data['id_delivery']) == 0) {
                return response (['message' => 'Metode Pengiriman Wajib Diisi'], 500);
            }

            $shop = DB::table('t_shops')->where('id_seller',auth()->user()->id)->where('is_active',1)->first();
            if ($shop == null) {
                return response (['message' => 'Maaf Anda belum mempunyai Toko'], 500);
            }

            $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$data['product_name']);
            $slug  = strtolower($slug);
            $price = intval(preg_replace('/\D/', '', $data['selling_price']));
            $sku_code = 'M'.date('YmdHis');
            $group_product = parent::generateRandomString(15,0);
            $last_price = $price;
            if (isset($data['sku_code'])) {
                if ($data['sku_code'] != null || $data['sku_code'] != '') {
                    $sku_code = $data['sku_code'];
                }
            }
            if (isset($data['discount'])) {
                if ($data['discount'] != 0 || $data['discount'] != '') {
                    $diskon = ($price * $data['discount']) / 100;
                    $last_price = $price - $diskon;
                }
            }

            $data_insert = ['product_name'=>$data['product_name'],
                            'selling_price'=>$price,
                            'last_price'=>$last_price,
                            'slug'=>$slug,
                            'type_of_weight'=>$data['type_of_weight'],
                            'weight'=>$data['weight'],
                            'width'=>$data['width'],
                            'heigth'=>$data['heigth'],
                            'length'=>$data['length'],
                            'stock'=>$data['stock'],
                            'minimum_order'=>$data['minimum_order'],
                            'pre_order'=>$data['pre_order'],
                            'id_shop'=>$shop->id,
                            'is_optional_assurance'=>$data['is_optional_assurance'],
                            'is_must_assurance'=>$data['is_must_assurance'],
                            'description'=>$data['description'],
                            'variant_model_name'=>$data['variant_model_name'],
                            'main_product'=> 1,
                            'group_product'=> $group_product,
                            'discount'=> $data['discount'],
                            'sku_code'=>$sku_code,
                            'created_at'=>date('Y-m-d H:i:s'),
                            'updated_at'=>date('Y-m-d H:i:s')
            ];

            $insert = DB::table('t_products')->insertGetId($data_insert);
            
            //relasi category product
            if (isset($data['id_category'])) {
                $category = $data['id_category'];
                if (count($category) > 0) {
                    foreach ($category as $k => $v) {
                        $insert_category = DB::table('relation_product_category')->insert(['id_product'=>$insert,'id_category'=>$category[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }
                
            }
            //relasi category product
            if (isset($data['id_delivery'])) {
                $delivery = $data['id_delivery'];
                if (count($delivery) > 0) {
                    foreach ($delivery as $k => $v) {
                        $insert_delivery = DB::table('relation_delivery_products')->insert(['id_product'=>$insert,'id_delivery'=>$delivery[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }
            }
            
            //relasi gudang product 
            if (isset($data['id_warehouse'])) {
                $warehouse = $data['id_warehouse'];
                if (count($warehouse) > 0) {
                    foreach ($warehouse as $k => $v) {
                        $insert_warehouse = DB::table('relation_product_warehouses')->insert(['product_id'=>$insert,'warehouse_id'=>$warehouse[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'stock'=>$data['stock']]);
                    }
                }
            }

            //relasi etalase product
            if (isset($data['id_etalase'])) {
                $etalse = $data['id_etalase'];
                if (count($etalse) > 0) {
                    foreach ($etalse as $k => $v) {
                        $insert_etalase = DB::table('relation_product_etalase')->insert(['id_product'=>$insert,'id_etalase'=>$etalse[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }
                
            }

            //relasi image
            if (isset($data['image'])) {
                $image = $data['image'];
                if (count($image) > 0) {
                    $no = 0;
                    $thumbnail = 0;
                    foreach ($image as $k => $v) {
                    $no = $no + 1;
                    if ($no == 1) {
                        $thumbnail = 1;
                    }
                        $insert_image = DB::table('t_product_images')->insert(['id_product'=>$insert,'image'=>$image[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'is_thumbnail'=>$thumbnail]);
                    }
                }
            }
            
            //jika memiliki vaiant
             if (isset($data['variant'])) {
                $variant = $data['variant'];
                if (count($variant) > 0) {
                    $no = 0;
                    foreach ($variant as $k => $v) {
                    $no = $no + 1;
                        $sku_code = 'M_'.$no.'_'.date('YmdHis');
                        $price = intval(preg_replace('/\D/', '', $variant[$k]['price']));
                        if (isset($variant[$k]['sku_code'])) {
                            if ($variant[$k]['sku_code'] != null || $variant[$k]['sku_code'] != '') {
                                $sku_code = $variant[$k]['sku_code'];
                            }
                        }
                        //insert variant
                        $data_variant = ['product_name'=>$data['product_name'],
                                'selling_price'=>$price,
                                'slug'=>$slug,
                                'type_of_weight'=>$data['type_of_weight'],
                                'weight'=>$variant[$k]['weight'],
                                // 'width'=>$variant[$k]['width'],
                                // 'heigth'=>$variant[$k]['heigth'],
                                // 'length'=>$variant[$k]['length'],
                                'stock'=>$variant[$k]['stock'],
                                'minimum_order'=>$data['minimum_order'],
                                'pre_order'=>$data['pre_order'],
                                'id_shop'=>$shop->id,
                                'is_optional_assurance'=>$data['is_optional_assurance'],
                                'is_must_assurance'=>$data['is_must_assurance'],
                                'description'=>$variant[$k]['description'],
                                'main_product'=> 0,
                                'group_product'=> $group_product,
                                'sku_code'=>$sku_code,
                                'variant_color'=>$variant[$k]['color'],
                                'variant_model'=>$variant[$k]['model'],
                                'created_at'=>date('Y-m-d H:i:s'),
                                'updated_at'=>date('Y-m-d H:i:s')
                            ];

                        $insert_variant = DB::table('t_products')->insertGetId($data_variant);
                        //image variant
                        $insert_image = DB::table('t_product_images')->insert(['id_product'=>$insert_variant,'image'=>$variant[$k]['image'],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'is_thumbnail'=>1]);
                        //relasi category product
                        if (isset($data['id_category'])) {
                            $category = $data['id_category'];
                            if (count($category) > 0) {
                                foreach ($category as $k => $v) {
                                    $insert_category = DB::table('relation_product_category')->insert(['id_product'=>$insert_variant,'id_category'=>$category[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                }
                            }
                            
                        }
                        //relasi category product
                        if (isset($data['id_delivery'])) {
                            $delivery = $data['id_delivery'];
                            if (count($delivery) > 0) {
                                foreach ($delivery as $k => $v) {
                                    $insert_delivery = DB::table('relation_delivery_products')->insert(['id_product'=>$insert_variant,'id_delivery'=>$delivery[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                }
                            }
                        }
                        
                        //relasi gudang product 
                        if (isset($data['id_warehouse'])) {
                            $warehouse = $data['id_warehouse'];
                            if (count($warehouse) > 0) {
                                foreach ($warehouse as $k => $v) {
                                    $insert_warehouse = DB::table('relation_product_warehouses')->insert(['product_id'=>$insert_variant,'warehouse_id'=>$warehouse[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'stock'=>$variant[$k]['stock']]);
                                }
                            }
                        }

                        //relasi etalase product
                        if (isset($data['id_etalase'])) {
                            $etalse = $data['id_etalase'];
                            if (count($etalse) > 0) {
                                foreach ($etalse as $k => $v) {
                                    $insert_etalase = DB::table('relation_product_etalase')->insert(['id_product'=>$insert_variant,'id_etalase'=>$etalse[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                }
                            }
                            
                        }
                    }
                }
            }

            $response = [
                'id' => $insert,
                'name' => $request->product_name,
                'message' => "Produk Baru Berhasil Ditambahkan"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@add');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //detail product
    public function detail_product(Request $request){
        try {
            $id_user = null;
            if (auth('sanctum')->check() == true) {
                $id_user = auth('sanctum')->user()->id;
            }
            //update jumlah pencarian
            $number = DB::table('t_products')->where('id',$request->id)->select('viewer')->first();
            if ($number != null) {
                $up_viewer = (int)$number->viewer + 1;
                $update = DB::table('t_products')->where('id',$request->id)->update(['viewer'=>$up_viewer]);
            }
            
            $data = DB::table('t_products')->where('id',$request->id)->select('id','product_name','slug','selling_price','last_price','discount','stock','rating','group_product','id_shop','description','pre_order','weight','width','length','heigth')->first();
            $data->person_have_cart =  parent::NumberCartProduct($data->id);
            $data->image = parent::AllImageProduct($data->id);
            $data->is_variant = 0;
            $data->variant = parent::VariantProduct($data->group_product);
            if (count($data->variant) > 0) {
                $data->is_variant = 1;
            }
            $data->review = parent::NumberReviewProduct($data->id);
            $data->cart = parent::NumberCartProduct($data->id);
            $data->shop_detail = $this->DataShop($data->id_shop);
            //insert data untuk dikelola
            $insertSystem = parent::insertSystem(1,$request->id,$id_user,$request->ip_device,$request->device_brand);
            $response = [
                'data' => $data
            ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@detail_product');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get shop detail
    public function DataShop($id){
        $data = DB::table('t_shops')->where('id',$id)->select('id','shop_name','shop_icon','rating','id_city','last_active')->first();
        if ($data->shop_icon != null) {
            $data->shop_icon = env('BASE_IMG').$data->shop_icon;
        }
        if ($data->id_city != null) {
            $data->id_city = parent::GetAddressName('cities',$data->id_city);
        }
        $data->is_last_active = 1;
        $data->text_last_active = "Active";
        $data->number_off_product = parent::ProductOfShop($data->id);
        $data->success_sales      = parent::SuccessSales($data->id);
        $time = new \DateTime($data->last_active);
        $diff = $time->diff(new \DateTime());
        $minutes = ($diff->days * 24 * 60) +
                   ($diff->h * 60) + $diff->i;
        $day = $minutes / 1440;
        if ($day >= 20) {
            $data->is_last_active = 0;
            $data->text_last_active = "Tidak Aktif ".(int)$day." hari yang lalu";
        }
        return $data;
    }

    //list_promotion
    public function list_promotion(Request $request){

        try {
            $code = 404;
            $data = DB::table('t_master_sales')->where('status',1)->where('end','>=',date('Y-m-d H:i:s'))->select('id','master_name','icon')->get();
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    if ($v->icon != null) {
                        $data[$k]->icon = env('BASE_IMG').$v->icon;
                    }
                }
            }
            $response = [
                'code' => $code,
                'data' => $data
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@list_promotion');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }

    }

    //data_promotion
    public function data_promotion(Request $request){

        try {
            $code = 404;
            $data = DB::table('t_master_sales')->where('is_flash_sale',0)->where('status',1)->where('end','>=',date('Y-m-d H:i:s'))->select('id','master_name','end','icon','background','is_flash_sale')->get();
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    if ($v->icon != null) {
                        $data[$k]->icon = env('BASE_IMG').$v->icon;
                    }
                    if ($v->background != null) {
                        $data[$k]->background = env('BASE_IMG').$v->background;
                    }
                    $data[$k]->product = $this->Productpromotion($v->id); 
                }
            }
            $response = [
                'code' => $code,
                'data' => $data
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@list_promotion');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }

    }

    //data_promotion
    public function data_promotion_flashsale(Request $request){

        try {
            $code = 404;
            $data = DB::table('t_master_sales')->where('is_flash_sale',1)->where('status',1)->where('end','>=',date('Y-m-d H:i:s'))->select('id','master_name','end','icon','background','is_flash_sale')->get();
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    if ($v->icon != null) {
                        $data[$k]->icon = env('BASE_IMG').$v->icon;
                    }
                    if ($v->background != null) {
                        $data[$k]->background = env('BASE_IMG').$v->background;
                    }
                    $data[$k]->product = $this->Productpromotion($v->id); 
                }
            }
            $response = [
                'code' => $code,
                'data' => $data
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@list_promotion');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }

    }

    // data produk dari promotion
    public function Productpromotion($id){
        $data = [];
        $in_array = DB::table('relation_product_sales')->where('id_master_sales',$id)->pluck('id_product')->toArray();
        $data = DB::table('t_products as p')->whereIn('id',$in_array)->where('status',1)->where('p.stock','>',0)->where('main_product',1)->select('p.id','product_name','slug','viewer','is_best_seller','discount','selling_price','last_price','sold_count','stock')->orderBy('created_at','desc')->limit(3)->get();
        if ($data->isNotEmpty()) {
            $code = 200;
            foreach ($data as $k => $v) {
                $data[$k]->category = parent::GetCaegoryProduct($v->id);
                $data[$k]->image = parent::GetImageProduct($v->id);
            }
        }

        return $data;
    }

    //detail_promotion
    public function detail_promotion(Request $request){

        try {
            $code = 404;
            //insert data untuk dikelola
            $id_user = null;
            if (auth('sanctum')->check() == true) {
                $id_user = auth('sanctum')->user()->id;
            }
            $insertSystem = parent::insertSystem(4,$request->id,$id_user,$request->ip_device,$request->device_brand);
            $nomor = DB::table('t_master_sales')->where('id',$request->id)->pluck('viewer');
            $up_viewer = (int)$nomor[0] + 1;
            $update = DB::table('t_master_sales')->where('id',$request->id)->update(['viewer'=>$up_viewer]);
            $in_array = DB::table('relation_product_sales')->where('id_master_sales',$request->id)->pluck('id_product')->toArray();
            $data = DB::table('t_products as p')->whereIn('id',$in_array)->where('status',1)->where('main_product',1)->select('p.id','product_name','slug','viewer','is_best_seller','discount','selling_price','last_price','sold_count')->orderBy('created_at','desc')->limit(3)->get();
            if ($data->isNotEmpty()) {
                $code = 200;
                foreach ($data as $k => $v) {
                    $data[$k]->category = parent::GetCaegoryProduct($v->id);
                    $data[$k]->image = parent::GetImageProduct($v->id);
                }
            }
            $response = [
                'code' => $code,
                'data' => $data
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@detail_promotion');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }

    }

    // click variant produk
    public function click_variant_produck(Request $request){
        try {
            $data = [];
            if ($request->type == 1) {
                $data = DB::table('t_products')->where('group_product',$request->group)->where('variant_color',$request->variant)->whereNotNull('variant_model')->where('variant_model','!=','')->select('variant_model as variant')->get();
                if ($data->isEmpty()) {
                    $data_x = DB::table('t_products')->where('group_product',$request->group)->where('variant_color',$request->variant)->select('id','selling_price as price','discount','stock')->first();
                    if ($data_x != null) {
                        $code = 200;
                        $data_x->image = parent::GetImageProduct($data_x->id);
                    }
                    
                    $response = [
                        'code' => $code,
                        'is_variant' => 0,
                        'data' => $data_x
                    ];
                    return response($response,200);
                }
            } else {
                $data = DB::table('t_products')->where('group_product',$request->group)->where('variant_model',$request->variant)->where('variant_color','!=','')->whereNotNull('variant_color')->select('variant_color as variant')->get();
                 if ($data->isEmpty()) {
                    $data_x = DB::table('t_products')->where('group_product',$request->group)->where('variant_model',$request->variant)->select('id','selling_price as price','discount','stock')->first();
                    if ($data_x != null) {
                        $code = 200;
                        $data_x->image = parent::GetImageProduct($data_x->id);
                    }
                    
                    $response = [
                        'code' => $code,
                        'is_variant' => 0,
                        'data' => $data_x
                    ];
                    return response($response,200);
                }
            }
            $response = [
                'is_variant' => 1,
                'data' => $data
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@click_variant_produck');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // click variant produk
    public function choose_variant_produck(Request $request){
        try {
            $data = [];
            $code = 404;
            $data = DB::table('t_products')->where('group_product',$request->group)->where('variant_color',$request->variant_color)->where('variant_model',$request->variant_model)->select('id','selling_price as price','discount','stock')->first();
            if ($data != null) {
                $code = 200;
                $data->image = parent::GetImageProduct($data->id);
            }
            
            $response = [
                'code' => $code,
                'data' => $data
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@choose_variant_produck');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // get id product
    public function get_id(Request $request){
        try {
            $data = [];
            $data = DB::table('t_products')->where('id',$request->id)->select('id','product_name','stock','selling_price','discount','type_of_weight','weight','sku_code','description','minimum_order','pre_order','is_optional_assurance','is_must_assurance','variant_model_name','group_product','length','width','heigth')->first();
            if ($data != null) {
                $data->is_variant = 0;
                $code = 200;
                $data->id_category = $this->CategoryProduct($data->id);
                $data->id_warehouse = $this->WarehouseProduct($data->id);
                $data->id_delivery = $this->DeliveryProduct($data->id);
                $data->id_etalase = $this->EtalaseProduct($data->id);
                $data->image = parent::AllImageProductWithId($data->id);
                $data->variant = $this->VariantProduct($data->group_product);
                if (count($data->variant) > 0) {
                    $data->is_variant = 1;
                }
            }
            
            $response = [
                'data' => $data
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@choose_variant_produck');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //category produk
    public function CategoryProduct($id){
        $data = DB::table('relation_product_category as r')->join('t_category_products as c','c.id','r.id_category')->where('id_product',$id)->select('id_category as id','category_product_name as name')->get();
        return $data;
    }

    //category produk
    public function EtalaseProduct($id){
        $data = DB::table('relation_product_etalase as r')->join('t_etalases as c','c.id','r.id_etalase')->where('id_product',$id)->select('id_etalase as id','etalase_name as name')->get();
        return $data;
    }

    //relasi produk gudang
    public function WarehouseProduct($id){
        $data = DB::table('relation_product_warehouses as r')->join('t_warehouses as c','c.id','r.warehouse_id')->where('product_id',$id)->select('warehouse_id as id','warehouses_name as name')->get();
        return $data;
    }

    //relasi pengiriman produk
    public function DeliveryProduct($id){
        $data = DB::table('relation_delivery_products as r')->join('t_master_deliveries as c','c.id','r.id_delivery')->where('id_product',$id)->select('id_delivery as id','master_delivery_name as name')->get();
        return $data;
    }

    // update product
    public function update(Request $request){
        try {
            $data = json_decode(request()->getContent(), true);
            
            //nama wajib diisi
            if ($data['product_name'] == null || $data['product_name'] == '') {
                return response (['message' => 'Nama Produk Wajib Diisi'], 500);
            }
            //id category wajib minimal 1
            if (count($data['id_category']) == 0) {
                return response (['message' => 'Kategori Wajib Diisi'], 500);
            }
            //nama wajib diisi
            if ($data['selling_price'] == null || $data['selling_price'] == '') {
                return response (['message' => 'Harga Wajib Diisi'], 500);
            }
            //nama wajib diisi
            if ($data['stock'] == null || $data['stock'] == '') {
                return response (['message' => 'Stock Produk Wajib Diisi'], 500);
            }
            //Gudangy wajib minimal 1
            if (count($data['id_warehouse']) == 0) {
                return response (['message' => 'Gudang Wajib Diisi'], 500);
            }
            //Delivery wajib minimal 1
            if (count($data['id_delivery']) == 0) {
                return response (['message' => 'Metode Pengiriman Wajib Diisi'], 500);
            }

            $shop = DB::table('t_shops')->where('id_seller',auth()->user()->id)->where('is_active',1)->first();
            if ($shop == null) {
                return response (['message' => 'Maaf Anda belum mempunyai Toko'], 500);
            }

            $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$data['product_name']);
            $slug  = strtolower($slug);
            $price = intval(preg_replace('/\D/', '', $data['selling_price']));
            $product = DB::table('t_products')->where('id',$request->id)->first();
            $last_price = $price;
            if (isset($data['discount'])) {
                if ($data['discount'] != 0 || $data['discount'] != '') {
                    $diskon = ($price * $data['discount']) / 100;
                    $last_price = $price - $diskon;
                }
            }

            $data_insert = ['product_name'=>$data['product_name'],
                            'selling_price'=>$price,
                            'last_price'=>$last_price,
                            'slug'=>$slug,
                            'type_of_weight'=>$data['type_of_weight'],
                            'weight'=>$data['weight'],
                            'width'=>$data['width'],
                            'heigth'=>$data['heigth'],
                            'length'=>$data['length'],
                            'stock'=>$data['stock'],
                            'minimum_order'=>$data['minimum_order'],
                            'pre_order'=>$data['pre_order'],
                            'is_optional_assurance'=>$data['is_optional_assurance'],
                            'is_must_assurance'=>$data['is_must_assurance'],
                            'description'=>$data['description'],
                            'variant_model_name'=>$data['variant_model_name'],
                            'discount'=> $data['discount'],
                            'sku_code'=>$data['sku_code'],
                            'updated_at'=>date('Y-m-d H:i:s')
            ];

            $insert = DB::table('t_products')->where('id',$request->id)->update($data_insert);
            
            //relasi category product
            DB::table('relation_product_category')->where('id_product',$request->id)->delete();
            if (isset($data['id_category'])) {
                $category = $data['id_category'];
                if (count($category) > 0) {
                    foreach ($category as $k => $v) {
                        $insert_category = DB::table('relation_product_category')->insert(['id_product'=>$request->id,'id_category'=>$category[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }
                
            }
            //relasi category product
            DB::table('relation_delivery_products')->where('id_product',$request->id)->delete();
            if (isset($data['id_delivery'])) {
                $delivery = $data['id_delivery'];
                if (count($delivery) > 0) {
                    foreach ($delivery as $k => $v) {
                        $insert_delivery = DB::table('relation_delivery_products')->insert(['id_product'=>$request->id,'id_delivery'=>$delivery[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }
            }
            
            //relasi gudang product 
            DB::table('relation_product_warehouses')->where('product_id',$request->id)->delete();
            if (isset($data['id_warehouse'])) {
                $warehouse = $data['id_warehouse'];
                if (count($warehouse) > 0) {
                    foreach ($warehouse as $k => $v) {
                        $insert_warehouse = DB::table('relation_product_warehouses')->insert(['product_id'=>$request->id,'warehouse_id'=>$warehouse[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'stock'=>$data['stock']]);
                    }
                }
            }

            //relasi etalase product
            DB::table('relation_product_etalase')->where('id_product',$request->id)->delete();
            if (isset($data['id_etalase'])) {
                $etalse = $data['id_etalase'];
                if (count($etalse) > 0) {
                    foreach ($etalse as $k => $v) {
                        $insert_etalase = DB::table('relation_product_etalase')->insert(['id_product'=>$request->id,'id_etalase'=>$etalse[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                    }
                }
                
            }

            //relasi image
            DB::table('t_product_images')->where('id_product',$request->id)->delete();
            if (isset($data['image'])) {
                $image = $data['image'];
                if (count($image) > 0) {
                    $no = 0;
                    $thumbnail = 0;
                    foreach ($image as $k => $v) {
                    $no = $no + 1;
                    if ($no == 1) {
                        $thumbnail = 1;
                    }
                        $insert_image = DB::table('t_product_images')->insert(['id_product'=>$request->id,'image'=>$image[$k],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'is_thumbnail'=>$thumbnail]);
                    }
                }
            }
            //dd("sampai sini");
            //jika memiliki vaiant
             if (isset($data['variant'])) {
                $variant = $data['variant'];
                if (count($variant) > 0) {
                    $no = 0;
                    foreach ($variant as $k => $v) {
                    $no = $no + 1;
                        $sku_code = 'M_'.$no.'_'.date('YmdHis');
                        $price_v = intval(preg_replace('/\D/', '', $variant[$k]['price']));
                        if ($variant[$k]['id'] != '' || $variant[$k]['id'] != null) {
                            $update_variant = DB::table('t_products')->where('id',$variant[$k]['id'])->update(['selling_price'=>$price_v,'stock'=>$variant[$k]['stock'],'weight'=>$variant[$k]['weight'],'variant_color'=>$variant[$k]['color'],'variant_model'=>$variant[$k]['model'],'description'=>$variant[$k]['description'],'sku_code'=>$variant[$k]['sku_code'],'updated_at'=>date('Y-m-d H:i:s')]);
                            $update_image = DB::table('t_product_images')->where('id_product',$variant[$k]['id'])->update(['image'=>$variant[$k]['image'],'updated_at'=>date('Y-m-d H:i:s')]);
                            //relasi category product
                            DB::table('relation_product_category')->where('id_product',$variant[$k]['id'])->delete();
                            if (isset($data['id_category'])) {
                                $category = $data['id_category'];
                                if (count($category) > 0) {
                                    foreach ($category as $c => $vey) {
                                        $insert_category = DB::table('relation_product_category')->insert(['id_product'=>$variant[$k]['id'],'id_category'=>$category[$c],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                    }
                                }
                                
                            }
                            //relasi category product
                            DB::table('relation_delivery_products')->where('id_product',$variant[$k]['id'])->delete();
                            if (isset($data['id_delivery'])) {
                                $delivery = $data['id_delivery'];
                                if (count($delivery) > 0) {
                                    foreach ($delivery as $d => $vey) {
                                        $insert_delivery = DB::table('relation_delivery_products')->insert(['id_product'=>$variant[$k]['id'],'id_delivery'=>$delivery[$d],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                    }
                                }
                            }
                            
                            //relasi gudang product 
                            DB::table('relation_product_warehouses')->where('product_id',$variant[$k]['id'])->delete();
                            if (isset($data['id_warehouse'])) {
                                $warehouse = $data['id_warehouse'];
                                if (count($warehouse) > 0) {
                                    foreach ($warehouse as $w => $vey) {
                                        $insert_warehouse = DB::table('relation_product_warehouses')->insert(['product_id'=>$variant[$k]['id'],'warehouse_id'=>$warehouse[$w],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'stock'=>$variant[$k]['stock']]);
                                    }
                                }
                            }

                            //relasi etalase product
                            DB::table('relation_product_etalase')->where('id_product',$variant[$k]['id'])->delete();
                            if (isset($data['id_etalase'])) {
                                $etalse = $data['id_etalase'];
                                if (count($etalse) > 0) {
                                    foreach ($etalse as $e => $vey) {
                                        $insert_etalase = DB::table('relation_product_etalase')->insert(['id_product'=>$variant[$k]['id'],'id_etalase'=>$etalse[$e],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                    }
                                }
                                
                            }
                        } else {
                            //insert variant
                            if ($variant[$k]['sku_code'] != '' || $variant[$k]['sku_code'] != null) {
                                $sku_code = $variant[$k]['sku_code'];
                            }
                            $data_variant = ['product_name'=>$data['product_name'],
                                    'selling_price'=>$price_v,
                                    'slug'=>$slug,
                                    'type_of_weight'=>$data['type_of_weight'],
                                    'weight'=>$variant[$k]['weight'],
                                    'stock'=>$variant[$k]['stock'],
                                    'minimum_order'=>$data['minimum_order'],
                                    'pre_order'=>$data['pre_order'],
                                    'id_shop'=>$shop->id,
                                    'is_optional_assurance'=>$data['is_optional_assurance'],
                                    'is_must_assurance'=>$data['is_must_assurance'],
                                    'description'=>$variant[$k]['description'],
                                    'main_product'=> 0,
                                    'group_product'=> $product->group_product,
                                    'sku_code'=>$sku_code,
                                    'variant_color'=>$variant[$k]['color'],
                                    'variant_model'=>$variant[$k]['model'],
                                    'created_at'=>date('Y-m-d H:i:s'),
                                    'updated_at'=>date('Y-m-d H:i:s')
                                ];

                            $insert_variant = DB::table('t_products')->insertGetId($data_variant);
                            //image variant
                            $insert_image = DB::table('t_product_images')->insert(['id_product'=>$insert_variant,'image'=>$variant[$k]['image'],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'is_thumbnail'=>1]);
                            //relasi pengiriman
                            if (isset($data['id_delivery'])) {
                                $delivery = $data['id_delivery'];
                                if (count($delivery) > 0) {
                                    foreach ($delivery as $d => $vey) {
                                        $insert_delivery = DB::table('relation_delivery_products')->insert(['id_product'=>$insert_variant,'id_delivery'=>$delivery[$d],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                    }
                                }
                            }
                            
                            //relasi gudang product 
                            if (isset($data['id_warehouse'])) {
                                $warehouse = $data['id_warehouse'];
                                if (count($warehouse) > 0) {
                                    foreach ($warehouse as $w => $vey) {
                                        $insert_warehouse = DB::table('relation_product_warehouses')->insert(['product_id'=>$insert_variant,'warehouse_id'=>$warehouse[$w],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'stock'=>$variant[$k]['stock']]);
                                    }
                                }
                            }

                            //relasi CATEGORY
                            if (isset($data['id_category'])) {
                                $category = $data['id_category'];
                                if (count($category) > 0) {
                                    foreach ($category as $c => $vey) {
                                        $insert_category = DB::table('relation_product_category')->insert(['id_product'=>$request->insert_variant,'id_category'=>$category[$c],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                    }
                                }
                                
                            }

                            //relasi etalase product
                            if (isset($data['id_etalase'])) {
                                $etalse = $data['id_etalase'];
                                if (count($etalse) > 0) {
                                    foreach ($etalse as $e => $vey) {
                                        $insert_etalase = DB::table('relation_product_etalase')->insert(['id_product'=>$request->insert_variant,'id_etalase'=>$etalse[$e],'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }

            $response = [
                'id' => $insert,
                'name' => $request->product_name,
                'message' => "Produk ".$request->product_name."Berhasil DiEdit"
            ];

            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // delete produk
    public function delete(Request $request){
        try {
            $message = 'Maaf Produk Tidak Bisa dihapus karena sudah dibeli oleh customer';
            $code = 404;
            $cek_transaksi = DB::table('t_detail_transactions')->where('id_product',$request->id)->get();
            $cek_cart = DB::table('t_carts')->where('id_product',$request->id)->get();
            if ($cek_transaksi->isEmpty() && $cek_cart->isEmpty()) {
                $code = 200;
                $message = 'Berhasil menghapus Produk';
                $delete = DB::table('t_products')->where('id',$request->id)->update(['deleted_at'=>date('Y-m-d'),'status'=>0]);
            }
            
            $response = [
                'code' => $code,
                'message' => $message
            ];
            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@delete');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // delete image product
    public function delete_image(Request $request){
        try {
            $image = str_replace("https://souq.s3-id-jkt-1.kilatstorage.id/","",$request->image);
            $delete = DB::table('t_product_images')->where('image',$image)->delete();
            $response = [
                'message' => "Berhasil menghapus gambar"
            ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@delete');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // get all variant product
    public function VariantProduct($group){
        $return = [];
        $data = DB::table('t_products')->where('group_product',$group)->where('main_product',0)->select('id','selling_price as price','variant_color as color','variant_model as model','stock','weight','description','sku_code')->get();
        if ($data->isNotEmpty()) {
            foreach ($data as $k => $v) {
                $data[$k]->image = parent::GetImageProductWithId($v->id);
            }
            $return = $data;
        }
        return $return;
    }

    //set_status
    public function set_status(Request $request){
        try {
            $update = DB::table('t_products')->where('id',$request->id)->update(['status'=>$request->status,'updated_at'=>date('Y-m-d H:i:s')]);
            $response = [
                'message' => "Berhasil mengubah status produk"
            ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ProductController@set_status');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
}

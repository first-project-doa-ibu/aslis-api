<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\User;

use Illuminate\Http\Response;
use Illuminate\Http\Request;
use DB;

class ShedController extends Controller
{
	// Home Index
    public function index(Request $request) {
    	try {
	        $code = 404;
            $shed = DB::table('t_sheds')->whereNull('deleted_at')->where('id_user',auth('sanctum')->user()->id);

            if ($request->id_province != null) {
                $shed = $shed->where('id_province',$request->id_province);
            }

            if ($request->id_city != null) {
                $shed = $shed->where('id_city',$request->id_city);
            }

            if ($request->search != null) {
                $shed = $shed->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('shed_name','ilike', "%{$search}%");
                    $query->orWhere('shed_id','ilike', "%{$search}%");
                });
            }

            if (!isset($request->sorting)){
                $shed = $shed->orderBy('created_at','desc');
            }

            if ($request->sorting == 1) {
                $shed = $shed->orderBy('shed_name','asc');
            }
            if ($request->sorting == 2) {
                $shed = $shed->orderBy('created_at','desc');
            }
            if ($request->sorting == 3) {
                $shed = $shed->orderBy('shed_id','asc');
            }

            $shed = $shed->select('id','shed_name','shed_id','number_of_cattle','weigth_of_cattle');
            $shed = $shed->paginate(10);

            if ($shed->isNotEmpty()) {
                $code = 200;
                foreach ($shed as $k => $v) {
                    $shed[$k]->number_of_cattle = $this->WeigthOfShed($v->id,1);
                    $weigth = $this->WeigthOfShed($v->id,2);
                    $shed[$k]->weigth_of_cattle = number_format((float)$weigth, 2, '.', '');
                }
            }

            $response = [
                'code' => $code,
                'data' => $shed
            ];

            return response($response,200);
    	} catch (\Exception $err) {
    		$error = $err->getMessage();
    		//Insert Log Error
    		parent::LogErrorCreate($error,'ShedController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    public function WeigthOfShed($id,$type){
        $return = 0;
        if ($type == 1) {
            $data = DB::table('t_shed_histories')->where('id_new_shed',$id)->where('status',1)->count();
            $return = (float)$data;
        } else {
            $data = DB::table('t_shed_histories')->where('id_new_shed',$id)->where('status',1)->pluck('id_cattle');
            if ($data->isNotEmpty()) {
                $sum = DB::table('t_cattles')->whereIn('id',$data->toArray())->sum('weigth');
                $return = (float)$sum;
            }
        }
        return $return;
    }


    //detail//
    public function detail(Request $request){
        try {
            $data = DB::table('t_sheds')->where('id',$request->id)->first();
            if($data != null){
                $data->city     = parent::GetAddressName('cities',$data->id_city);
                $data->province = parent::GetAddressName('provinces',$data->id_province);
                $data->number_of_cattle = $this->WeigthOfShed($data->id,1);
                $data->weigth_of_cattle = $this->WeigthOfShed($data->id,2);
            }
            $response = [
                'data' =>$data,
                'message' => "Get Data"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShedController@detail');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // tambah kandang
    public function add(Request $request) {
        try {
            $input = $request->except('_token');

            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_sheds')->insertGetId($input);

            $response = [
                'data' => $input,
                'message' => "Berhasil menambah kandang"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShedController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // update kandang
    public function update(Request $request) {
        try {
            $input = $request->except('_token');
            $input['id_user'] = auth()->user()->id;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_sheds')->where('id',$request->id)->update($input);

            $response = [
                'data' => $input,
                'message' => "Berhasil menambah kandang"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShedController@update');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // delete kandang
    public function delete(Request $request) {
        try {

            $insert = DB::table('t_sheds')->where('id',$request->id)->update(['status'=>0,'deleted_at'=>date('Y-m-d')]);

            $response = [
                'message' => "Berhasil menghapus kandang"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShedController@delete');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // set status kandang
    public function set_status(Request $request) {
        try {

            $insert = DB::table('t_sheds')->where('id',$request->id)->update(['status'=>$request->status,'updated_at'=>date('Y-m-d H:i:s')]);

            $response = [
                'message' => "Berhasil menghapus kandang"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShedController@set_status');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //history
    public function history(Request $request) {
        try {
            $code = 404;
            $cattle = DB::table('t_shed_histories as h')->join('t_cattles as c','c.id','h.id_cattle')->whereNull('h.deleted_at')->where('c.id_user',auth('sanctum')->user()->id);

            if ($request->id_kandang != null) {
                $in_array = DB::table('t_shed_histories')->where('id_old_shed',$request->id_kandang)->orWhere('id_new_shed',$request->id_kandang)->pluck('id_cattle')->toArray();
                $cattle = $cattle->whereIn('c.id',$in_array);
            }

            if ($request->search != null) {
                $cattle = $cattle->where(function ($query) use ($request) {
                    $search = $request->search;
                    $query->where('cattle_name','ilike', "%{$search}%");
                    $query->orWhere('cattle_id','ilike', "%{$search}%");
                });
            }

            if ($request->id_cattle != null) {
                $cattle = $cattle->where('c.id',$request->id_cattle);
            }

            $cattle = $cattle->select('c.id','cattle_name','gender','weigth','birthday','photo','id_old_shed','id_new_shed','h.description')->orderBy('h.created_at','desc');

            if (!isset($request->sorting)){
                $cattle = $cattle->orderBy('h.created_at','desc');
            }

            if ($request->sorting == 1) {
                $cattle = $cattle->orderBy('cattle_name','asc');
            }
            if ($request->sorting == 2) {
                $cattle = $cattle->orderBy('h.created_at','desc');
            }
            if ($request->sorting == 3) {
                $cattle = $cattle->orderBy('h.created_at','asc');
            }

            $cattle = $cattle->paginate(10);

            if ($cattle->isNotEmpty()) {
                $code = 200;
                foreach ($cattle as $k => $v) {
                    if ($v->photo != null) {
                        $cattle[$k]->photo = env('BASE_IMG').$v->photo;
                    } else {
                        $cattle[$k]->photo = parent::ImageCattle($v->id);
                    }

                    $cattle[$k]->kandang_lama = $this->Shedname($v->id_old_shed);
                    $cattle[$k]->kandang_baru = $this->Shedname($v->id_new_shed);
                    $cattle[$k]->age = parent::getAge($v->birthday);
                }
            }

            $response = [
                'code' => $code,
                'data' => $cattle
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShedController@history');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    public function Shedname($id){
        $return = "-";
        $data = DB::table('t_sheds')->where('id',$id)->pluck('shed_name');
        if ($data->isNotEmpty()) {
            $return = $data[0];
        }
        return $return;
    }
}

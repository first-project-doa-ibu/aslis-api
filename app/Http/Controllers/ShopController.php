<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Shops;
use App\Models\User;
use App\Models\WareHouse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class ShopController extends Controller
{
    public function index(Request $request) {
        $data = [];
        $is_seller = 0;
    	$data = Shops::where('is_active',1)->where('id_seller',auth()->user()->id)->first();
        if ($data != null) {
            $is_seller = 1;
            $data->shop_icon = env('BASE_IMG').$data->shop_icon;
        }
        
    	$response = [
            'is_seller' =>$is_seller,
    		'data' => $data,
    		'messages' => "Success"
    	];

    	return response($response,200);
    }

    // verifikasi Akun sebagai Seller

    public function verifikasi_akun(Request $request) {

    	try {
            
            $upload_ktp  = null;
            $selfie_ktp  = null;
            $is_verified = 0;
            $this->validate($request, ['upload_ktp' => 'required|image']);
            if($request->hasfile('upload_ktp'))
            {
                $upload_ktp = parent::uploadFileS3($request->file('upload_ktp'));
            }

            if($request->hasfile('selfie_ktp'))
            {
                $selfie_ktp = parent::uploadFileS3($request->file('selfie_ktp'));
                $is_verified = 1;
            }

            $update = User::where('id',auth()->user()->id)->update(['upload_nik'=>$upload_ktp,'selfie_nik'=>$selfie_ktp,'is_verified'=>$is_verified]);

            $response = [
                'messages' => "Pengajuan Verifikasi Akun Anda Berhasil"
            ];
            parent::LogSystem('Berhasil megajukan verifikasi akun seller',auth()->user()->id,'Pengajuan Verifikasi Akun Seller',2,null);

            return response($response,200);

    	} catch (\Exception $err) {
    		$error = $err->getMessage();
    		//Insert Log Error
    		parent::LogErrorCreate($error,'HomeController@index');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // register shop (Daftar toko)

    public function register(Request $request) {

        try {
            $fields = $request->validate([
                'shop_name' => 'required|string'
            ]);
            $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$request->shop_name);
            $slug  = strtolower($slug);
            
            $upload_ktp  = null;
            $selfie_ktp  = null;
            $is_verified = 0;
            $icon = null;
            if($request->hasfile('icon'))
            {
                $icon = parent::uploadFileS3($request->file('icon'));
            }

            $insert = DB::table('t_shops')->insertGetId([
                        'shop_name'=>$request->shop_name,
                        'shop_icon'=>$icon,
                        'slug'=>$slug,
                        'description'=>$request->description,
                        'domain'=>$request->domain,
                        'id_seller'=>auth()->user()->id]);
            //insert category shop
            $category = $request->id_category;
            if (isset($request->id_category)) {
                foreach ($category as $a => $b) {
                    $insertrel = [
                        'shop_id' => $insert,
                        'category_id' => $category[$a],
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=>date('Y-m-d H:i:s'),
                    ];
                    $insert2 = DB::table('relation_category_shops')->insert($insertrel);
                }
            }

            $response = [
                'messages' => "Toko Anda Berhasil"
            ];
            parent::LogSystem('Berhasil membuat toko baru',auth()->user()->id,'Membuat Toko',2,$insert);

            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShopController@register');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // data gudang
    public function get_warehouse(Request $request) {

    	try {
	        $data = WareHouse::whereNull('deleted_at')->where('id_user',auth()->user()->id)->select('id','warehouses_name','pos_code','latitude','longitude')->get();
	    	$response = [
	    		'data' => $data
	    	];

	    	return response($response,200);
    	} catch (\Exception $err) {
    		$error = $err->getMessage();
    		//Insert Log Error
    		parent::LogErrorCreate($error,'ShopController@get_warehouse');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //tambah category shop
    public function add_category(Request $request) {

        try {
            $fields = $request->validate([
                'category_name' => 'required|string'
            ]);
            $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$request->category_name);
            $slug  = strtolower($slug);
            $icon = null;
            if($request->hasfile('icon'))
            {
                $icon = parent::uploadFileS3($request->file('icon'));
            }
            $insert = DB::table('t_category_shops')->insertGetId(['category_shop_name'=>$request->category_name,'description'=>$request->description,'slug'=>$slug,'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s'),'category_shop_icon'=>$icon]);
            $response = [
                'id' => $insert,
                'name' => $request->category_name
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShopController@add_category');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }
    
    //tambah master delivery
    public function add_master_delivery(Request $request) {

        try {
            $fields = $request->validate([
                'delivery_name' => 'required|string',
                'price' => 'required|string',
                'maximum_distance' => 'required|integer'
            ]);
            $price = intval(preg_replace('/\D/', '', $request->price));
            $icon = null;
            if($request->hasfile('icon'))
            {
                $icon = parent::uploadFileS3($request->file('icon'));
            }
            
            $insert = DB::table('t_master_deliveries')->insertGetId(['master_delivery_name'=>$request->delivery_name,'description'=>$request->description,'maximum_distance'=>$request->maximum_distance,'id_user'=>auth()->user()->id,'id_warehouse'=>$request->id_warehouse,'price'=>$price,'master_delivery_icon'=>$icon,'master_delivery_code'=>'MD'.date('YmdHis'),'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            $response = [
                'id' => $insert,
                'name' => $request->delivery_name,
                'message' => "Berhasil menambah metode pengiriman sendiri"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShopController@add_master_delivery');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //update master delivery
    public function update_master_delivery(Request $request) {

        try {
            $fields = $request->validate([
                'delivery_name' => 'required|string',
                'price' => 'required|string',
                'maximum_distance' => 'required|integer'
            ]);
            $price = intval(preg_replace('/\D/', '', $request->price));
            //$icon = null;
            $existing = DB::table('t_master_deliveries')->where('id',$request->id)->first();
            if($request->hasfile('icon'))
            {
                $icon = parent::uploadFileS3($request->file('icon'));
            } else {
                $icon = $existing->master_delivery_icon;
            }
            
            $update = DB::table('t_master_deliveries')->where('id',$request->id)->update(['master_delivery_name'=>$request->delivery_name,'description'=>$request->description,'maximum_distance'=>$request->maximum_distance,'id_user'=>auth()->user()->id,'id_warehouse'=>$request->id_warehouse,'price'=>$price,'master_delivery_icon'=>$icon,'master_delivery_code'=>'MD'.date('YmdHis'),'created_at'=>date('Y-m-d H:i:s'),'updated_at'=>date('Y-m-d H:i:s')]);
            $response = [
                'id' => $request->id,
                'name' => $request->delivery_name,
                'message' => "Berhasil mengedit metode pengiriman sendiri"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShopController@update_master_delivery');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //delete master delivery
    public function delete_master_delivery(Request $request) {

        try {
            
            $update = DB::table('t_master_deliveries')->where('id',$request->id)->update(['deleted_at'=>date('Y-m-d'),'status'=>0]);
            $response = [
                'id' => $request->id,
                'name' => $request->delivery_name,
                'message' => "Berhasil mengedit metode pengiriman sendiri"
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShopController@delete_master_delivery');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // cek domain
    public function check_domain(Request $request) {
        
        try {
            $fields = $request->validate([
                'domain' => 'required|string'
            ]);

            $cek = Shops::where('domain',$request->domain)->whereNull('deleted_at')->get();
            $code = 500;
            $messages = "Maaf Domain tersebut sudah ada yang menggunakan";

            if ($cek->isEmpty()) {
                $code = 200;
                $messages = "Domain tersedia";
            }
            
            $response = [
                'code' => $code,
                'messages' => $messages
            ];

            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'ShopController@add_master_delivery');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // master delivery custom
    public function master_delivery() {
        
        $data = DB::table('t_master_deliveries as m')->leftJoin('t_warehouses as w','w.id','m.id_warehouse')->where('m.status',1)->where('m.id_user',auth()->user()->id)->select('m.id','master_delivery_name as name','master_delivery_code','master_delivery_icon','is_cod','warehouses_name','w.address','maximum_distance','price')->orderBy('master_delivery_name','asc')->get();
        foreach ($data as $k => $v) {
            if ($v->master_delivery_icon != null) {
                $data[$k]->icon = env('BASE_IMG').$v->master_delivery_icon;
            } else {
                $data[$k]->icon = null;
            }
        }
        $response = [
            'data' => $data,
            'messages' => "Success"
        ];
        return response($response,200);
    }

    // detail master delivery custom
    public function detail_master_delivery(Request $request) {
        
        $data = DB::table('t_master_deliveries as m')->leftJoin('t_warehouses as w','w.id','m.id_warehouse')->where('m.id',$request->id)->where('m.id_user',auth()->user()->id)->select('m.id','master_delivery_name as name','id_warehouse','master_delivery_code','master_delivery_icon','master_delivery_icon as icon','is_cod','warehouses_name','w.address','maximum_distance','price','m.description')->first();
        if ($data != null) {
            if (isset($data->master_delivery_icon)) {
                $data->icon = env('BASE_IMG').$data->master_delivery_icon;
            } else {
                $data->icon = null;
            }
            $messages = "success";
        } else {
            $messages = "Mohon Maaf data ini bukan punya Anda";
        }
        //foreach ($data as $k => $v) {
           
        //}
        $response = [
            'data' => $data,
            'messages' => $messages,
        ];
        return response($response,200);
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\Shops;
use App\Models\User;
use App\Models\WareHouse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class StoreController extends Controller
{
    public function index(Request $request) {

    	$data = Shops::where('is_active',1)->where('id_seller',auth()->user()->id)->select('id','shop_name','shop_icon','followers','shop_level')->first();
        $data->shop_icon = env('BASE_IMG').$data->shop_icon;
        if ($data->shop_level == 1) {
            $data->shop_level = "Juragan baru";
        } else if ($data->shop_level == 1) {
            $data->shop_level = "Juragan Berpengalaman";
        } else if ($data->shop_level == 1) {
            $data->shop_level = "Super Seller Juragan";
        } else {
            $data->shop_level = "Status Belum ditentukan";
        } 
        $data->total_feedback = $this->TotalFeedbcak($data->id); 
        $data->positif_feedback = $this->PositifFeedbcak($data->id); 
        $data->avg_order_processing = $this->AvgProcessing($data->id); 
    	$response = [
    		'data' => $data,
    		'messages' => "Success"
    	];

    	return response($response,200);
    }

    // Total feeedback
    public function TotalFeedbcak($id){
        $return = 0;
        $id_product = Product::whereNull('deleted_at')->where('id_shop',$id)->pluck('id')->toArray();
        $data = DB::table('t_user_feedbacks')->whereIn('id_product',$id_product)->count();

        return $data;
    }

    public function PositifFeedbcak($id){
        $return = 0;
        $id_product = Product::whereNull('deleted_at')->where('id_shop',$id)->pluck('id')->toArray();
        $data = DB::table('t_user_feedbacks')->whereIn('id_product',$id_product)->where('feedback_value','>',50)->count();

        return $data;
    }

    public function AvgProcessing($id){
        $trans = DB::table('t_transactions')->where('id_shop',$id)->pluck('id')->toArray();
        $data = DB::table('t_order_processing')->whereIn('id_transaction',$trans)->avg('count_of_day');
        return $data;
    }


    // Menambahkan Etalase Baru
    public function add_etalase(Request $request) {

        try {
            $fields = $request->validate([
                'etalase_name' => 'required|string'
            ]);
            $input = $request->except(['_token','isi']);
            $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$request->etalase_name);
            $slug  = strtolower($slug);
            $input['slug'] = $slug;
            $input['created_at'] = date('Y-m-d H:i:s');
            $input['updated_at'] = date('Y-m-d H:i:s');
            $input['id_user']    = auth()->user()->id;
            $insert = DB::table('t_etalases')->insertGetId($input);
            $response = [
                'name'  => $request->etalase_name,
                'messages' => "Berhasil menambah Etalase"
            ];
            parent::LogSystem('Berhasil membuat etalase baru',auth()->user()->id,'Membuat Etalase baru',2,$insert);

            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'StoreController@add_etalase');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // Update Etalase Baru
    public function update_etalase(Request $request) {

        try {
            $fields = $request->validate([
                'etalase_name' => 'required|string'
            ]);
            $input = $request->except(['_token','isi']);
            $slug = str_replace(array(' ', '/', '(', ')', '[', ']', '?', ':', ';','<', '>', '&', '{', '}', '*'), array('-'),$request->etalase_name);
            $slug  = strtolower($slug);
            $input['slug'] = $slug;
            $input['updated_at'] = date('Y-m-d H:i:s');
            $insert = DB::table('t_etalases')->where('id',$request->id)->update($input);
            $response = [
                'name'  => $request->etalase_name,
                'messages' => "Berhasil menambah Etalase"
            ];
            parent::LogSystem('Berhasil megedit etalase ',auth()->user()->id,'Mengedit Etalase',2,$request->id);

            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'StoreController@update_etalase');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // Deletee Etalase
    public function delete_etalase(Request $request) {

        try {
            
            $insert = DB::table('t_etalases')->where('id',$request->id)->update(['deleted_at'=>date('Y-m-d')]);
            $response = [
                'messages' => "Berhasil menghapus Etalase"
            ];
            parent::LogSystem('Berhasil meghapus etalase ',auth()->user()->id,'Menghapus Etalase',2,$request->id);

            return response($response,200);

        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'StoreController@delete_etalase');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    // data etalase
    public function get_etalase(Request $request) {

    	try {
	        $data = DB::table('t_etalases')->whereNull('deleted_at')->where('id_user',auth()->user()->id)->select('id','etalase_name','slug')->orderBy('etalase_name','asc')->get();
	    	$response = [
	    		'data' => $data
	    	];

	    	return response($response,200);
    	} catch (\Exception $err) {
    		$error = $err->getMessage();
    		//Insert Log Error
    		parent::LogErrorCreate($error,'StoreController@get_etalase');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //get product 
    public function get_product(Request $request) {

        try {
            
            $data = $this->GetData($request);
            $data = $data->select('id','product_name','slug','viewer','is_best_seller','discount','selling_price','sold_count','stock','sku_code','status','reason','suspend_date_active');
            if ($request->sort == 1) {
                $data = $data->orderBy('created_at','desc');
            }
            if ($request->sort == 2) {
                $data = $data->orderBy('sold_count','desc');
            } 
            if ($request->sort == 3) {
                $data = $data->orderBy('selling_price','desc');
            }
            $data = $data->paginate(6);
            foreach ($data as $k => $v) {
                $data[$k]->category = parent::GetCaegoryProduct($v->id);
                $data[$k]->image = parent::GetImageProduct($v->id);
                if ($v->status == 0) {
                    $data[$k]->text_status = "Tidak Aktif";
                } else if ($v->status == 1) {
                    $data[$k]->text_status = "Aktif";
                } else if ($v->status == 2) {
                    $data[$k]->text_status = "Produk Disuspend";
                } else {
                    $data[$k]->text_status = "Produk Di Banned";
                }
            }
            $response = [
                'data' => $data
            ];
            return response($response,200);
        } catch (\Exception $err) {
            $error = $err->getMessage();
            //Insert Log Error
            parent::LogErrorCreate($error,'StoreController@get_product');
            return response([
                'message' => 'ERROR ' . $err->getMessage(),
            ], 500);
        }
    }

    //Data Function
    public function GetData($request){
        $shop = DB::table('t_shops')->where('id_seller',auth()->user()->id)->where('is_active',1)->first();
        $data = Product::whereNull('deleted_at')->where('id_shop',$shop->id)->where('main_product',1)->where(function ($query) use ($request) {
                $search = $request->search;
                if ($request->search != null) {
                    $query->where(function ($query2) use ($search) {
                        $query2->where('product_name','ilike', "%{$search}%");
                        $query2->orWhere('variant_color','ilike', "%{$search}%");
                        $query2->orWhere('variant_size','ilike', "%{$search}%");
                        $query2->orWhere('variant_model','ilike', "%{$search}%");
                    });
                } 
                if ($request->id_category != null) {
                    $in_array_category = DB::table('relation_product_category')->where('id_category',$request->id_category)->pluck('id_product')->toArray();
                    $query->whereIn('id',$in_array_category);
                } 
                if ($request->id_etalase != null) {
                    $in_array_etalase = DB::table('relation_product_etalase')->where('id_etalase',$request->id_etalase)->pluck('id_product')->toArray();
                    $query->whereIn('id',$in_array_etalase);
                } 
        });
        return $data;
    }

    // get category product
    public function get_category_product() {
        
        $shop = DB::table('t_shops')->where('id_seller',auth()->user()->id)->where('is_active',1)->first();
        $id_product = Product::whereNull('deleted_at')->where('id_shop',$shop->id)->where('main_product',1)->pluck('id')->toArray();
        $data = DB::table('relation_product_category as r')->join('t_category_products as c','c.id','r.id_category')->whereIn('id_product',$id_product)->select('c.id','category_product_name as name','slug')->distinct('c.id')->get();
        $response = [
            'data' => $data
        ];
        return response($response,200);
    }
}

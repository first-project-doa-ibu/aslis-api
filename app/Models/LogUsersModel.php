<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LogUsersModel extends Model
{
    use HasFactory;
    protected $table 	= 't_log_users';
    protected $guarded = [''];
    public $timestamps = true;
    public $incrementing = false;
    protected $keyType = 'uuid';
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shops extends Model
{
    use HasFactory;
    protected $table 	= 't_shops';
    protected $guarded = [''];
    public $incrementing = false;
    public $timestamps = true;
    protected $keyType = 'uuid';
}

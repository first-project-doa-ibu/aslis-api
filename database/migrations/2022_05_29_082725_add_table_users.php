<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('user_name')->nullable();
            $table->string('user_email')->nullable();
            $table->string('user_phone')->nullable();
            $table->string('user_nik')->nullable();
            $table->string('upload_nik')->nullable();
            $table->string('selfie_nik')->nullable();
            $table->string('password')->nullable();
            $table->string('confirm_password')->nullable();
            $table->integer('status')->default(1);
            $table->date('birthday')->nullable();
            $table->string('profile')->nullable();
            $table->string('id_country')->nullable();
            $table->string('id_province')->nullable();
            $table->string('id_city')->nullable();
            $table->string('id_district')->nullable();
            $table->string('address')->nullable();
            $table->string('pos_code')->nullable();
            $table->integer('gender')->default(1);
            $table->string('firebase_android')->nullable();
            $table->string('firebase_ios')->nullable();
            $table->integer('otp')->nullable();
            $table->timestamp('otp_request_time')->nullable();
            $table->timestamp('last_active')->nullable();
            $table->integer('request_otp_number')->nullable();
            $table->integer('is_valid_register')->default(0);
            $table->integer('is_verified')->default(0);
            $table->integer('register_method')->default(1);
            $table->timestamp('suspend_date_ctive')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
        DB::statement('ALTER TABLE t_users ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_users');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_administrators', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('admin_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('password')->nullable();
            $table->string('confirm_password')->nullable();
            $table->integer('status')->default(1);
            $table->uuid('id_role')->nullable();
            $table->string('profile')->nullable();
            $table->string('address')->nullable();
            $table->integer('gender')->default(1);
            $table->timestamp('suspend_date_ctive')->nullable();
            $table->text('reason')->nullable();
            $table->timestamps();
            $table->date('deleted_at')->nullable();
        });
        DB::statement('ALTER TABLE t_administrators ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_administrators');
    }
};

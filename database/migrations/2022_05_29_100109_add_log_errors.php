<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_log_errors', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_user')->nullable();
            $table->uuid('id_admin')->nullable();
            $table->string('controller')->nullable();
            $table->integer('line_error')->default(0);
            $table->text('exception')->nullable();
            $table->integer('tipe_user')->default(1);
            $table->integer('is_view')->default(0);
            $table->integer('is_solved')->default(0);
            $table->timestamp('failed_at')->nullable();
        });
        DB::statement('ALTER TABLE t_log_errors ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_log_errors');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";');
        Schema::create('t_log_admins', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('id_admin')->nullable();
            $table->string('menu')->nullable();
            $table->text('activity')->nullable();
            $table->string('mac_address')->nullable();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE t_log_admins ALTER COLUMN id SET DEFAULT uuid_generate_v4();');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_log_admins');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_devices', function (Blueprint $table) {
            $table->id();
            $table->uuid('id_user');
            $table->foreign('id_user')->references('id')->on('t_users');
            $table->integer('type');
            $table->string('device_name')->nullable();
            $table->string('device_id')->nullable();
            $table->text('post');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_devices');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_sheds', function (Blueprint $table) {
            $table->id();
            $table->uuid('id_user');
            $table->foreign('id_user')->references('id')->on('t_users');
            $table->integer('type');
            $table->text('post');
            $table->integer('status')->default(0);
            $table->string('shed_name');
            $table->string('shed_id')->nullable();
            $table->integer('number_of_cattle')->default(0);
            $table->decimal('weigth_of_cattle', 10, 2)->default(0);
            $table->foreignId('id_city')->nullable();
            $table->foreign('id_city')->references('id')->on('cities');
            $table->foreignId('id_province')->nullable();
            $table->foreign('id_province')->references('id')->on('provinces');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_sheds');
    }
};

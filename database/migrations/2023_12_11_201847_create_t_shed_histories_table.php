<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_shed_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_old_shed');
            $table->foreign('id_old_shed')->references('id')->on('t_sheds');
            $table->unsignedBigInteger('id_new_shed');
            $table->foreign('id_new_shed')->references('id')->on('t_sheds');
            $table->unsignedBigInteger('id_cattle');
            $table->foreign('id_cattle')->references('id')->on('t_cattles');
            $table->integer('status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_shed_histories');
    }
};

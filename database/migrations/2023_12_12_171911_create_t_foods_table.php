<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_foods', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('type');
            $table->string('post');
            $table->integer('available_stock');
            $table->decimal('price', 8, 2);
            $table->unsignedBigInteger('id_category');
            $table->foreign('id_category')->references('id')->on('t_food_categories');
            $table->uuid('id_user');
            $table->string('food_name');
            $table->string('food_id');
            $table->string('category_name');
            $table->decimal('capital_price', 8, 2);
            $table->integer('stock');
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_foods');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_cattles', function (Blueprint $table) {
            $table->bigInteger('cattle_id')->after('id_user')->nullable();
            $table->string('cattle_name')->after('cattle_id')->nullable();
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_cattles', function (Blueprint $table) {
            $table->dropColumn('cattle_id');
            $table->dropColumn('cattle_name');
            $table->dropColumn('status');
        });
    }
};

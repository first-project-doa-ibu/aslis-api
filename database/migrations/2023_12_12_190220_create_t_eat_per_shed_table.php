<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_eat_per_shed', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_food');
            $table->unsignedBigInteger('id_shed');
            $table->uuid('id_user');
            $table->timestamps();
            $table->foreign('id_food')->references('id')->on('t_foods');
            $table->foreign('id_shed')->references('id')->on('t_sheds');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_eat_per_shed');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_cattles', function (Blueprint $table) {
            $table->string('gender')->nullable();
            $table->date('birthday')->nullable();
            $table->string('photo')->nullable();
            $table->unsignedBigInteger('id_category')->nullable();
            $table->foreign('id_category')->references('id')->on('t_food_categories');
            $table->boolean('is_selling')->default(false);
            $table->decimal('selling_price', 8, 2)->nullable();
            $table->decimal('discount', 8, 2)->nullable();
            $table->decimal('final_price', 8, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_cattles', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('birthday');
            $table->dropColumn('photo');
            $table->dropForeign(['id_category']);
            $table->dropColumn('id_category');
            $table->dropColumn('is_selling');
            $table->dropColumn('selling_price');
            $table->dropColumn('discount');
            $table->dropColumn('final_price');
        });
    }
};

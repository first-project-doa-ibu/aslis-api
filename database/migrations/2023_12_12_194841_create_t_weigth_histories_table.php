<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_weigth_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_cattle');
            $table->unsignedBigInteger('id_shed') ;
            $table->unsignedBigInteger('id_history')->nullable();
            $table->float('weigth');
            $table->tinyInteger('is_new')->default(1);
            $table->string('photo')->nullable();
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_weigth_histories');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_cattles', function (Blueprint $table) {
            $table->string('other_field')->nullable();
            $table->string('post')->nullable();
            $table->date('input_date')->nullable();
            $table->unsignedBigInteger('id_old_shed')->nullable();
            $table->unsignedBigInteger('id_history')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_cattles', function (Blueprint $table) {
            $table->dropColumn('other_field');
            $table->dropColumn('post');
            $table->dropColumn('input_date');
            $table->dropColumn('id_old_shed');
            $table->dropColumn('id_history');
        });
    }
};

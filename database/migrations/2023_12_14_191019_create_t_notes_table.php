<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_notes', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('content');
            $table->uuid('id_user');
            $table->integer('type');
            $table->string('post');
            $table->string('photo')->nullable();
            $table->date('date');
            $table->integer('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_notes');
    }
};

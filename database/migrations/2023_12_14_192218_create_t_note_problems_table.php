<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_note_problems', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_note');
            $table->timestamps();
            $table->foreign('id_note')->references('id')->on('t_notes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_note_problems');
    }
};

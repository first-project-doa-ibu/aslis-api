<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_notifications', function (Blueprint $table) {
            $table->id();
            $table->uuid('id_user')->nullable();
            $table->foreign('id_user')->references('id')->on('t_users')->onDelete('cascade');
            $table->boolean('is_umum')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_notifications');
    }
};

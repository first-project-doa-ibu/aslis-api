<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user_read_notive', function (Blueprint $table) {
            $table->id();
            $table->uuid('id_user');
            $table->unsignedBigInteger('id_notification');
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('t_users');
            $table->foreign('id_notification')->references('id')->on('t_notifications');
            $table->timestamp('read_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_read_notive');
    }
};

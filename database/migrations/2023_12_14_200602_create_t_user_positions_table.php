<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_user_positions', function (Blueprint $table) {
            $table->id();
            $table->integer('type');
            $table->text('post')->nullable();
            $table->uuid('id_user');
            $table->foreign('id_user')->references('id')->on('t_users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_user_positions');
    }
};

<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\GetDataController;
use App\Http\Controllers\ShedController;
use App\Http\Controllers\CattleController;
use App\Http\Controllers\NotesController;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/login',[AuthController::class,'index_login'])->name('login');
Route::prefix('auth')->group(function () {
	Route::post('register',[AuthController::class,'register']);
	Route::post('otp_validation',[AuthController::class,'otp_validation']);
	Route::post('send_otp',[AuthController::class,'send_otp']);
    Route::post('login',[AuthController::class,'login']);
    Route::post('login_sosmed',[AuthController::class,'login_sosmed']);
});


//private Route
Route::group(['middleware'=> ['auth:sanctum']], function () {

	Route::post('/home', [GetDataController::class,'home']);
    Route::get('/laporan_pakan', [GetDataController::class,'laporan_pakan']);
    Route::get('/laporan_ternak', [GetDataController::class,'laporan_ternak']);

	Route::prefix('auth')->group(function () {
	    Route::post('update_phone',[AuthController::class,'update_phone']);
	    Route::get('/get_data_profile',[AuthController::class,'get_data_profile']);
	    Route::post('/update_profile',[AuthController::class,'update_profile']);
	    Route::post('/update_phone',[AuthController::class,'update_phone']);
	    Route::post('/update_phone/otp_validation',[AuthController::class,'otp_validation2']);
	    Route::post('/update_phone/send_otp',[AuthController::class,'send_otp2']);
	    Route::post('logout',[AuthController::class,'logout']);
	    Route::post('add_device',[AuthController::class,'add_device']);
	    Route::post('delete_device',[AuthController::class,'delete_device']);
	    Route::post('get_device',[AuthController::class,'get_device']);
	});

	Route::post('upload_images',[AuthController::class,'upload_images']);

	// Group Sheds/kandang
	Route::prefix('shed')->group(function () {
		//Route Home With AUTH
	    Route::post('/index', [ShedController::class,'index']);
	    Route::post('/add', [ShedController::class,'add']);
	    Route::post('/detail', [ShedController::class,'detail']);
	    Route::post('/set_status', [ShedController::class,'set_status']);
	    Route::post('/update', [ShedController::class,'update']);
	    Route::post('/delete', [ShedController::class,'delete']);
	    Route::post('/history', [ShedController::class,'history']);
	});

	// Group pakan
	Route::prefix('food')->group(function () {
		//Route Home With AUTH
	    Route::post('/index', [FoodController::class,'index']);
	    Route::post('/add', [FoodController::class,'add']);
	    Route::post('/detail', [FoodController::class,'detail']);
	    Route::post('/choose', [FoodController::class,'choose']);
	    Route::post('/set_status', [FoodController::class,'set_status']);
	    Route::post('/update', [FoodController::class,'update']);
	    Route::post('/delete', [FoodController::class,'delete']);
	    Route::post('/history', [FoodController::class,'history']);
	    Route::post('/history_shed', [FoodController::class,'history_shed']);
	    Route::post('/detail_history', [FoodController::class,'detail_history']);
	    Route::post('/update_history', [FoodController::class,'update_history']);
	    Route::post('/delete_history', [FoodController::class,'delete_history']);
	});

	Route::prefix('get_data')->group(function () {
		Route::get('/shed', [GetDataController::class,'shed']);
		Route::get('/cattle', [GetDataController::class,'cattle']);
		Route::get('/food', [GetDataController::class,'food']);
		Route::get('/female_parent', [GetDataController::class,'female_parent']);
        Route::get('/male_parent', [GetDataController::class,'male_parent']);
	});

    //Binatang Ternak
	Route::prefix('cattle')->group(function () {
		//Route Home With AUTH
	    Route::post('/index', [CattleController::class,'index']);
	    Route::post('/detail', [CattleController::class,'detail']);
	    Route::post('/set_status', [CattleController::class,'set_status']);
	    Route::post('/delete', [CattleController::class,'delete']);
	    Route::post('/search', [CattleController::class,'search']);
	    Route::post('/move_shed', [CattleController::class,'move_shed']);
	    Route::post('/weigth_noted', [CattleController::class,'weigth_noted']);
	    Route::post('/weigth_noted_from_scan', [CattleController::class,'weigth_noted_from_scan']);
	    Route::post('/give_food', [CattleController::class,'give_food']);
	    Route::post('/weigth_history', [CattleController::class,'weigth_history']);
	    Route::post('/weigth_history_shed', [CattleController::class,'weigth_history_shed']);
	    Route::post('/detail_weigth_history', [CattleController::class,'detail_weigth_history']);
	    Route::post('/update_weigth_history', [CattleController::class,'update_weigth_history']);
	    Route::post('/delete_weigth_history', [CattleController::class,'delete_weigth_history']);
	    Route::post('/delete_file', [CattleController::class,'delete_file']);
	    Route::post('/sale', [CattleController::class,'sale']);
	    Route::post('/unsale', [CattleController::class,'unsale']);

	    Route::prefix('internal')->group(function () {
	    	Route::post('/add', [CattleController::class,'add']);
	    	Route::post('/update', [CattleController::class,'update']);
	    });
	    Route::prefix('eksternal')->group(function () {
	    	Route::post('/add', [CattleController::class,'add']);
	    	Route::post('/update', [CattleController::class,'update']);
	    });
	});

	// catatan
	Route::prefix('notes')->group(function () {
		//Route Home With AUTH
	    Route::post('/index', [NotesController::class,'index']);
	    Route::post('/add', [NotesController::class,'add']);
	    Route::post('/detail', [NotesController::class,'detail']);
	    Route::post('/set_status', [NotesController::class,'set_status']);
	    Route::post('/update', [NotesController::class,'update']);
	    Route::post('/delete', [NotesController::class,'delete']);
	});

	Route::prefix('notification')->group(function () {
		//Route Home
	    Route::get('/index', [NotificationController::class,'index']);
	    Route::post('/delete', [NotificationController::class,'delete']);
	});
});


// Get data umum
Route::prefix('get_data')->group(function () {
    //address
	Route::prefix('address')->group(function () {
        Route::get('/country', [GetDataController::class,'country']);
        Route::get('/province', [GetDataController::class,'province']);
        Route::get('/city/{id}', [GetDataController::class,'city']);
        Route::get('/district/{id}', [GetDataController::class,'district']);
        Route::get('/village/{id}', [GetDataController::class,'village']);
    });

    Route::get('/category_cattle', [GetDataController::class,'category_cattle']);
    Route::get('/category_food', [GetDataController::class,'category_food']);
    Route::get('/master_events', [GetDataController::class,'master_events']);
    Route::get('/master_problems', [GetDataController::class,'master_problems']);

});

Route::post('/update_position', [GetDataController::class,'update_position']);
Route::post('/weigth_noted_from_machine', [GetDataController::class,'weigth_noted_from_machine']);
Route::post('/upload_csv', [GetDataController::class,'upload_csv']);
